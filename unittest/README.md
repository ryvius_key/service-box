# 测试框架功能

```
./service-box-unittest -h
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/132237_f612c489_467198.png "屏幕截图.png")


# 运行条件
1. 安装Zookeeper<br>
   启动配置中监听:127.0.0.1:2181, Zookeeper配置：zoo.cfg<br>

```
tickTime=2000
initLimit=10
syncLimit=5
clientPort=2181
maxClientCnxns=64
minSessionTimeout=5000
maxSessionTimeout=10000
dataDir=配置一个你的目录
zookeeper.client.sasl=false

```

2. 安装Nginx或者Apache等Http Service<br>
Nginx配置：nginx.conf<br>

```
worker_processes  1;
events {
    worker_connections  1024;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            autoindex on;
        }        
        location /doc {
            default_type application/octet-stream;
            autoindex on;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}

```
   TBD(用例使用的资源上传脚本).

# 运行

```
./service-box-unittest
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/132409_482bbb5c_467198.png "屏幕截图.png")