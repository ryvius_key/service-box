# 编写Lua语言服务

Lua服务的lua脚本会被bundle加载，每个lua服务都与bundle一一对应的关系。

## 编写服务定义

假设有一个服务定义, 定义在文件名为example.idl（接口描述文件请参考:[IDL服务定义](https://gitee.com/dennis-kk/rpc-frontend/blob/master/README.md)）内，example.idl存放于src/repo/example目录内:


```
service dynamic LuaLogin generic {
    [script_type:lua]

    ui64 login(string, string)
    void logout(ui64)
    oneway void oneway_method()
    void normal_method()
}
```

服务名为LuaLogin，有4个方法。

## 生成lua服务

在src/repo目录下运行:

```
python repo.py -t cpp -b example:LuaLogin
python repo.py -t lua -b example:LuaLogin
```

## 实现功能

在src/repo/usr/lua/5871407833815435533_lua/script目录下, 是LuaLogin服务的lua脚本实现文件。

## 修改并重新生成服务

打开src/repo/idl/example.idl，修改完毕后在src/repo下运行:

```
python repo.py -t lua -u example:LuaLogin
python repo.py -t lua -b example:LuaLogin
```

