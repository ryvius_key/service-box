#include "box_config.hh"

auto kratos::config::BoxConfig::get_string(const std::string &name)
    -> std::string {
  auto *attribute = get_config_ptr()->get(name);
  if (!attribute->isString()) {
    throw std::runtime_error("Attribute type is not string:" + name);
  }
  return attribute->string()->get();
}
