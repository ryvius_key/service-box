import glob
import os

def files(curr_dir = '.', ext = '*.exe'):
    for i in glob.glob(os.path.join(curr_dir, ext)):
        yield i

def remove_files(rootdir, ext, show = False):
    for i in files(rootdir, ext):
        os.remove(i)
        
def list_idl():
    idls = []
    for i in files("src/idl", "*.idl"):
        idls.append(os.path.basename(i))
    return idls