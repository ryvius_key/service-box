// Machine generated code

#region Machine generated code

using kratos;
using RingBuffer;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace rpc {

public abstract class Dummy {
    public abstract int field1 { get; set; }
    public abstract string field2 { get; set; }
    public abstract IList<string> field3 { get; }
    public abstract ISet<string> field4 { get; }
    public abstract IDictionary<long,string> field5 { get; }
    public abstract bool field6 { get; set; }
    public abstract float field7 { get; set; }
    public abstract double field8 { get; set; }
    public static Dummy Create() {
        return new DummyImpl();
    }
}

public abstract class Data {
    public abstract int field1 { get; set; }
    public abstract string field2 { get; set; }
    public abstract IList<string> field3 { get; }
    public abstract ISet<string> field4 { get; }
    public abstract IDictionary<long,string> field5 { get; }
    public abstract bool field6 { get; set; }
    public abstract float field7 { get; set; }
    public abstract double field8 { get; set; }
    public abstract Dummy field9 { get; }
    public abstract IList<Dummy> field10 { get; }
    public abstract ISet<string> field11 { get; }
    public abstract IDictionary<long,Dummy> field12 { get; }
    public static Data Create() {
        return new DataImpl();
    }
}

}

namespace rpc {

internal class DummyImpl : Dummy {
    Example.Dummy value_;
    private HashSet<string> inter_field4 = new HashSet<string>();
    public DummyImpl() {
        value_ = new Example.Dummy();
    }
    internal DummyImpl(Example.Dummy v) {
        value_ = v;
        foreach (var k in value_.Field4) {
            inter_field4.Add(k);
        }
    }
    internal Example.Dummy PBObject {
        get {
            foreach(var k in inter_field4) {
                value_.Field4.Add(k);
            }
            return value_;
        }
        set {
            value_ = value;
            foreach (var k in value_.Field4) {
                inter_field4.Add(k);
            }
        }
    }
    public override int field1 { get { return value_.Field1; } set { value_.Field1 = value; } }
    public override string field2 { get { return value_.Field2; } set { value_.Field2 = value; } }
    public override IList<string> field3 { get { return (IList<string>)value_.Field3; } }
    public override ISet<string> field4 { get { return (ISet<string>)inter_field4; } }
    public override IDictionary<long,string> field5 { get { return (IDictionary<long,string>)value_.Field5; } }
    public override bool field6 { get { return value_.Field6; } set { value_.Field6 = value; } }
    public override float field7 { get { return value_.Field7; } set { value_.Field7 = value; } }
    public override double field8 { get { return value_.Field8; } set { value_.Field8 = value; } }
}

internal class DataImpl : Data {
    Example.Data value_;
    private HashSet<string> inter_field4 = new HashSet<string>();
    private DummyImpl inter_field9 = new DummyImpl();
    private List<Dummy> inter_field10 = new List<Dummy>();
    private HashSet<string> inter_field11 = new HashSet<string>();
    private Dictionary<long,Dummy> inter_field12 = new Dictionary<long,Dummy>();
    public DataImpl() {
        value_ = new Example.Data();
    }
    internal DataImpl(Example.Data v) {
        value_ = v;
        foreach (var k in value_.Field4) {
            inter_field4.Add(k);
        }
        inter_field9.PBObject = value_.Field9;
        foreach (var k in value_.Field10) {
            inter_field10.Add(new DummyImpl(k));
        }
        foreach (var k in value_.Field11) {
            inter_field11.Add(k);
        }
        foreach (var k in value_.Field12) {
            inter_field12.Add(k.Key, new DummyImpl(k.Value));
        }
    }
    internal Example.Data PBObject {
        get {
            foreach(var k in inter_field4) {
                value_.Field4.Add(k);
            }
            value_.Field9 = inter_field9.PBObject;
            foreach (var k in inter_field10) {
                value_.Field10.Add(((DummyImpl)k).PBObject);
            }
            foreach(var k in inter_field11) {
                value_.Field11.Add(k);
            }
            foreach (var k in inter_field12) {
                value_.Field12.Add(k.Key, ((DummyImpl)k.Value).PBObject);
            }
            return value_;
        }
        set {
            value_ = value;
            foreach (var k in value_.Field4) {
                inter_field4.Add(k);
            }
            inter_field9.PBObject = value_.Field9;
            foreach (var k in value_.Field10) {
                inter_field10.Add(new DummyImpl(k));
            }
            foreach (var k in value_.Field11) {
                inter_field11.Add(k);
            }
            foreach (var k in value_.Field12) {
                inter_field12.Add(k.Key, new DummyImpl(k.Value));
            }
        }
    }
    public override int field1 { get { return value_.Field1; } set { value_.Field1 = value; } }
    public override string field2 { get { return value_.Field2; } set { value_.Field2 = value; } }
    public override IList<string> field3 { get { return (IList<string>)value_.Field3; } }
    public override ISet<string> field4 { get { return (ISet<string>)inter_field4; } }
    public override IDictionary<long,string> field5 { get { return (IDictionary<long,string>)value_.Field5; } }
    public override bool field6 { get { return value_.Field6; } set { value_.Field6 = value; } }
    public override float field7 { get { return value_.Field7; } set { value_.Field7 = value; } }
    public override double field8 { get { return value_.Field8; } set { value_.Field8 = value; } }
    public override Dummy field9{ get { return inter_field9; } }
    public override IList<Dummy> field10 { get { return inter_field10; } }
    public override ISet<string> field11 { get { return (ISet<string>)inter_field11; } }
    public override IDictionary<long,Dummy> field12 { get { return inter_field12; } }
}

}

namespace rpc {

internal static class Service_method1_delegate {
    public static Service_method1_CallParam NewParam() {
        return kratos.ObjectPool<Service_method1_CallParam>.Get();
    }
    public static Service_method1_CallReturn NewReturn() {
        return kratos.ObjectPool<Service_method1_CallReturn>.Get();
    }
}

internal class Service_method1_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method1_CallParam>.Recycle;
    }
    public Data arg1;
    public string arg2;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallParamSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallParamSerializer.method1_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = null;
        arg2 = null;
    }
}

internal static class Service_method2_delegate {
    public static Service_method2_CallParam NewParam() {
        return kratos.ObjectPool<Service_method2_CallParam>.Get();
    }
    public static Service_method2_CallReturn NewReturn() {
        return kratos.ObjectPool<Service_method2_CallReturn>.Get();
    }
}

internal class Service_method2_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method2_CallParam>.Recycle;
    }
    public sbyte arg1;
    public HashSet<string> arg2;
    public ulong arg3;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallParamSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallParamSerializer.method2_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
        arg2.Clear();
        arg3 = 0;
    }
}

internal static class Service_method3_delegate {
    public static Service_method3_CallParam NewParam() {
        return kratos.ObjectPool<Service_method3_CallParam>.Get();
    }
    public static Service_method3_CallReturn NewReturn() {
        return kratos.ObjectPool<Service_method3_CallReturn>.Get();
    }
}

internal class Service_method3_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method3_CallParam>.Recycle;
    }
    public sbyte arg1;
    public HashSet<string> arg2;
    public Dictionary<long,string> arg3;
    public ulong arg4;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallParamSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallParamSerializer.method3_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
        arg2.Clear();
        arg3.Clear();
        arg4 = 0;
    }
}

internal static class Service_method4_delegate {
    public static Service_method4_CallParam NewParam() {
        return kratos.ObjectPool<Service_method4_CallParam>.Get();
    }
    public static Service_method4_CallReturn NewReturn() {
        return kratos.ObjectPool<Service_method4_CallReturn>.Get();
    }
}

internal class Service_method4_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method4_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallParamSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallParamSerializer.method4_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class Service_method5_delegate {
    public static Service_method5_CallParam NewParam() {
        return kratos.ObjectPool<Service_method5_CallParam>.Get();
    }
    public static Service_method5_CallReturn NewReturn() {
        return kratos.ObjectPool<Service_method5_CallReturn>.Get();
    }
}

internal class Service_method5_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method5_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallParamSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallParamSerializer.method5_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

}

namespace rpc {

internal static class ServiceDynamic_method1_delegate {
    public static ServiceDynamic_method1_CallParam NewParam() {
        return kratos.ObjectPool<ServiceDynamic_method1_CallParam>.Get();
    }
    public static ServiceDynamic_method1_CallReturn NewReturn() {
        return kratos.ObjectPool<ServiceDynamic_method1_CallReturn>.Get();
    }
}

internal class ServiceDynamic_method1_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method1_CallParam>.Recycle;
    }
    public Data arg1;
    public string arg2;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallParamSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallParamSerializer.method1_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = null;
        arg2 = null;
    }
}

internal static class ServiceDynamic_method2_delegate {
    public static ServiceDynamic_method2_CallParam NewParam() {
        return kratos.ObjectPool<ServiceDynamic_method2_CallParam>.Get();
    }
    public static ServiceDynamic_method2_CallReturn NewReturn() {
        return kratos.ObjectPool<ServiceDynamic_method2_CallReturn>.Get();
    }
}

internal class ServiceDynamic_method2_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method2_CallParam>.Recycle;
    }
    public sbyte arg1;
    public HashSet<string> arg2;
    public ulong arg3;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallParamSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallParamSerializer.method2_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
        arg2.Clear();
        arg3 = 0;
    }
}

internal static class ServiceDynamic_method3_delegate {
    public static ServiceDynamic_method3_CallParam NewParam() {
        return kratos.ObjectPool<ServiceDynamic_method3_CallParam>.Get();
    }
    public static ServiceDynamic_method3_CallReturn NewReturn() {
        return kratos.ObjectPool<ServiceDynamic_method3_CallReturn>.Get();
    }
}

internal class ServiceDynamic_method3_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method3_CallParam>.Recycle;
    }
    public sbyte arg1;
    public HashSet<string> arg2;
    public Dictionary<long,string> arg3;
    public ulong arg4;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallParamSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallParamSerializer.method3_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
        arg2.Clear();
        arg3.Clear();
        arg4 = 0;
    }
}

internal static class ServiceDynamic_method4_delegate {
    public static ServiceDynamic_method4_CallParam NewParam() {
        return kratos.ObjectPool<ServiceDynamic_method4_CallParam>.Get();
    }
    public static ServiceDynamic_method4_CallReturn NewReturn() {
        return kratos.ObjectPool<ServiceDynamic_method4_CallReturn>.Get();
    }
}

internal class ServiceDynamic_method4_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method4_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallParamSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallParamSerializer.method4_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class ServiceDynamic_method5_delegate {
    public static ServiceDynamic_method5_CallParam NewParam() {
        return kratos.ObjectPool<ServiceDynamic_method5_CallParam>.Get();
    }
    public static ServiceDynamic_method5_CallReturn NewReturn() {
        return kratos.ObjectPool<ServiceDynamic_method5_CallReturn>.Get();
    }
}

internal class ServiceDynamic_method5_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method5_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallParamSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallParamSerializer.method5_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

}

namespace rpc {

internal static class Login_login_delegate {
    public static Login_login_CallParam NewParam() {
        return kratos.ObjectPool<Login_login_CallParam>.Get();
    }
    public static Login_login_CallReturn NewReturn() {
        return kratos.ObjectPool<Login_login_CallReturn>.Get();
    }
}

internal class Login_login_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Login_login_CallParam>.Recycle;
    }
    public string arg1;
    public string arg2;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834537456905; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LoginCallParamSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LoginCallParamSerializer.login_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = null;
        arg2 = null;
    }
}

internal static class Login_logout_delegate {
    public static Login_logout_CallParam NewParam() {
        return kratos.ObjectPool<Login_logout_CallParam>.Get();
    }
    public static Login_logout_CallReturn NewReturn() {
        return kratos.ObjectPool<Login_logout_CallReturn>.Get();
    }
}

internal class Login_logout_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Login_logout_CallParam>.Recycle;
    }
    public ulong arg1;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834537456905; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LoginCallParamSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LoginCallParamSerializer.logout_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
    }
}

}

namespace rpc {

internal static class LuaLogin_login_delegate {
    public static LuaLogin_login_CallParam NewParam() {
        return kratos.ObjectPool<LuaLogin_login_CallParam>.Get();
    }
    public static LuaLogin_login_CallReturn NewReturn() {
        return kratos.ObjectPool<LuaLogin_login_CallReturn>.Get();
    }
}

internal class LuaLogin_login_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_login_CallParam>.Recycle;
    }
    public string arg1;
    public string arg2;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallParamSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallParamSerializer.login_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = null;
        arg2 = null;
    }
}

internal static class LuaLogin_logout_delegate {
    public static LuaLogin_logout_CallParam NewParam() {
        return kratos.ObjectPool<LuaLogin_logout_CallParam>.Get();
    }
    public static LuaLogin_logout_CallReturn NewReturn() {
        return kratos.ObjectPool<LuaLogin_logout_CallReturn>.Get();
    }
}

internal class LuaLogin_logout_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_logout_CallParam>.Recycle;
    }
    public ulong arg1;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallParamSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallParamSerializer.logout_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
    }
}

internal static class LuaLogin_oneway_method_delegate {
    public static LuaLogin_oneway_method_CallParam NewParam() {
        return kratos.ObjectPool<LuaLogin_oneway_method_CallParam>.Get();
    }
    public static LuaLogin_oneway_method_CallReturn NewReturn() {
        return kratos.ObjectPool<LuaLogin_oneway_method_CallReturn>.Get();
    }
}

internal class LuaLogin_oneway_method_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_oneway_method_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallParamSerializer.oneway_method_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallParamSerializer.oneway_method_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class LuaLogin_normal_method_delegate {
    public static LuaLogin_normal_method_CallParam NewParam() {
        return kratos.ObjectPool<LuaLogin_normal_method_CallParam>.Get();
    }
    public static LuaLogin_normal_method_CallReturn NewReturn() {
        return kratos.ObjectPool<LuaLogin_normal_method_CallReturn>.Get();
    }
}

internal class LuaLogin_normal_method_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_normal_method_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallParamSerializer.normal_method_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallParamSerializer.normal_method_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

}

namespace rpc {

internal static class MyService_method1_delegate {
    public static MyService_method1_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method1_CallParam>.Get();
    }
    public static MyService_method1_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method1_CallReturn>.Get();
    }
}

internal class MyService_method1_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method1_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method1_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method2_delegate {
    public static MyService_method2_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method2_CallParam>.Get();
    }
    public static MyService_method2_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method2_CallReturn>.Get();
    }
}

internal class MyService_method2_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method2_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method2_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method3_delegate {
    public static MyService_method3_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method3_CallParam>.Get();
    }
    public static MyService_method3_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method3_CallReturn>.Get();
    }
}

internal class MyService_method3_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method3_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method3_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method4_delegate {
    public static MyService_method4_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method4_CallParam>.Get();
    }
    public static MyService_method4_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method4_CallReturn>.Get();
    }
}

internal class MyService_method4_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method4_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method4_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method5_delegate {
    public static MyService_method5_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method5_CallParam>.Get();
    }
    public static MyService_method5_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method5_CallReturn>.Get();
    }
}

internal class MyService_method5_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method5_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method5_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method6_delegate {
    public static MyService_method6_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method6_CallParam>.Get();
    }
    public static MyService_method6_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method6_CallReturn>.Get();
    }
}

internal class MyService_method6_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method6_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 6; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method6_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method6_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method7_delegate {
    public static MyService_method7_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method7_CallParam>.Get();
    }
    public static MyService_method7_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method7_CallReturn>.Get();
    }
}

internal class MyService_method7_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method7_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 7; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method7_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method7_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method8_delegate {
    public static MyService_method8_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method8_CallParam>.Get();
    }
    public static MyService_method8_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method8_CallReturn>.Get();
    }
}

internal class MyService_method8_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method8_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 8; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method8_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method8_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method9_delegate {
    public static MyService_method9_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method9_CallParam>.Get();
    }
    public static MyService_method9_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method9_CallReturn>.Get();
    }
}

internal class MyService_method9_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method9_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 9; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method9_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method9_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method10_delegate {
    public static MyService_method10_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method10_CallParam>.Get();
    }
    public static MyService_method10_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method10_CallReturn>.Get();
    }
}

internal class MyService_method10_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method10_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 10; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method10_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method10_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method11_delegate {
    public static MyService_method11_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method11_CallParam>.Get();
    }
    public static MyService_method11_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method11_CallReturn>.Get();
    }
}

internal class MyService_method11_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method11_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 11; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method11_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method11_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method12_delegate {
    public static MyService_method12_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method12_CallParam>.Get();
    }
    public static MyService_method12_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method12_CallReturn>.Get();
    }
}

internal class MyService_method12_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method12_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 12; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method12_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method12_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method13_delegate {
    public static MyService_method13_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method13_CallParam>.Get();
    }
    public static MyService_method13_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method13_CallReturn>.Get();
    }
}

internal class MyService_method13_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method13_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 13; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method13_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method13_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method14_delegate {
    public static MyService_method14_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method14_CallParam>.Get();
    }
    public static MyService_method14_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method14_CallReturn>.Get();
    }
}

internal class MyService_method14_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method14_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 14; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method14_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method14_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method15_delegate {
    public static MyService_method15_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method15_CallParam>.Get();
    }
    public static MyService_method15_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method15_CallReturn>.Get();
    }
}

internal class MyService_method15_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method15_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 15; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method15_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method15_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method16_delegate {
    public static MyService_method16_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method16_CallParam>.Get();
    }
    public static MyService_method16_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method16_CallReturn>.Get();
    }
}

internal class MyService_method16_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method16_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 16; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method16_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method16_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method17_delegate {
    public static MyService_method17_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method17_CallParam>.Get();
    }
    public static MyService_method17_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method17_CallReturn>.Get();
    }
}

internal class MyService_method17_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method17_CallParam>.Recycle;
    }
    public bool NoParam { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 17; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method17_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method17_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
    }
}

internal static class MyService_method18_delegate {
    public static MyService_method18_CallParam NewParam() {
        return kratos.ObjectPool<MyService_method18_CallParam>.Get();
    }
    public static MyService_method18_CallReturn NewReturn() {
        return kratos.ObjectPool<MyService_method18_CallReturn>.Get();
    }
}

internal class MyService_method18_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method18_CallParam>.Recycle;
    }
    public sbyte arg1;
    public short arg2;
    public int arg3;
    public long arg4;
    public byte arg5;
    public ushort arg6;
    public uint arg7;
    public ulong arg8;
    public bool arg9;
    public string arg10;
    public float arg11;
    public double arg12;
    public Data arg13;
    public List<Data> arg14;
    public List<string> arg15;
    public Dictionary<uint,Data> arg16;
    public HashSet<string> arg17;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 18; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallParamSerializer.method18_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallParamSerializer.method18_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
        arg2 = 0;
        arg3 = 0;
        arg4 = 0;
        arg5 = 0;
        arg6 = 0;
        arg7 = 0;
        arg8 = 0;
        arg9 = false;
        arg10 = null;
        arg11 = 0;
        arg12 = 0;
        arg13 = null;
        arg14.Clear();
        arg15.Clear();
        arg16.Clear();
        arg17.Clear();
    }
}

}

namespace rpc {

internal static class Lua_login_delegate {
    public static Lua_login_CallParam NewParam() {
        return kratos.ObjectPool<Lua_login_CallParam>.Get();
    }
    public static Lua_login_CallReturn NewReturn() {
        return kratos.ObjectPool<Lua_login_CallReturn>.Get();
    }
}

internal class Lua_login_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Lua_login_CallParam>.Recycle;
    }
    public string arg1;
    public string arg2;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833380299500; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaCallParamSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaCallParamSerializer.login_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = null;
        arg2 = null;
    }
}

internal static class Lua_logout_delegate {
    public static Lua_logout_CallParam NewParam() {
        return kratos.ObjectPool<Lua_logout_CallParam>.Get();
    }
    public static Lua_logout_CallReturn NewReturn() {
        return kratos.ObjectPool<Lua_logout_CallReturn>.Get();
    }
}

internal class Lua_logout_CallParam : CallParam, kratos.IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    bool debug_loop_ = false;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Lua_logout_CallParam>.Recycle;
    }
    public ulong arg1;
    public bool NoParam { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833380299500; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaCallParamSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaCallParamSerializer.logout_serialize(this);
    }
    public bool IsDebugLoop {
        get => debug_loop_;
        set => debug_loop_ = value;
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        arg1 = 0;
    }
}

}

namespace rpc {

internal class Service_method1_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method1_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallReturnSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallReturnSerializer.method1_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class Service_method2_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method2_CallReturn>.Recycle;
    }
    public string ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallReturnSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallReturnSerializer.method2_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class Service_method3_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method3_CallReturn>.Recycle;
    }
    public Dictionary<long,Dummy> ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallReturnSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallReturnSerializer.method3_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class Service_method4_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method4_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallReturnSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallReturnSerializer.method4_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class Service_method5_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Service_method5_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834219999997; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceCallReturnSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceCallReturnSerializer.method5_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

}

namespace rpc {

internal class ServiceDynamic_method1_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method1_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallReturnSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallReturnSerializer.method1_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class ServiceDynamic_method2_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method2_CallReturn>.Recycle;
    }
    public string ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallReturnSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallReturnSerializer.method2_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class ServiceDynamic_method3_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method3_CallReturn>.Recycle;
    }
    public Dictionary<long,Dummy> ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallReturnSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallReturnSerializer.method3_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class ServiceDynamic_method4_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method4_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallReturnSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallReturnSerializer.method4_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class ServiceDynamic_method5_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<ServiceDynamic_method5_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834711899994; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        ServiceDynamicCallReturnSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return ServiceDynamicCallReturnSerializer.method5_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

}

namespace rpc {

internal class Login_login_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Login_login_CallReturn>.Recycle;
    }
    public ulong ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834537456905; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LoginCallReturnSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LoginCallReturnSerializer.login_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class Login_logout_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Login_logout_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834537456905; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LoginCallReturnSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LoginCallReturnSerializer.logout_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

}

namespace rpc {

internal class LuaLogin_login_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_login_CallReturn>.Recycle;
    }
    public ulong ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallReturnSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallReturnSerializer.login_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class LuaLogin_logout_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_logout_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallReturnSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallReturnSerializer.logout_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class LuaLogin_oneway_method_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_oneway_method_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallReturnSerializer.oneway_method_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallReturnSerializer.oneway_method_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

internal class LuaLogin_normal_method_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<LuaLogin_normal_method_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833815435533; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaLoginCallReturnSerializer.normal_method_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaLoginCallReturnSerializer.normal_method_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

}

namespace rpc {

internal class MyService_method1_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method1_CallReturn>.Recycle;
    }
    public sbyte ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method1_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method1_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method2_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method2_CallReturn>.Recycle;
    }
    public short ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method2_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method2_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method3_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method3_CallReturn>.Recycle;
    }
    public int ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 3; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method3_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method3_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method4_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method4_CallReturn>.Recycle;
    }
    public long ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 4; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method4_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method4_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method5_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method5_CallReturn>.Recycle;
    }
    public byte ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 5; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method5_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method5_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method6_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method6_CallReturn>.Recycle;
    }
    public ushort ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 6; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method6_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method6_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method7_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method7_CallReturn>.Recycle;
    }
    public uint ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 7; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method7_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method7_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method8_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method8_CallReturn>.Recycle;
    }
    public ulong ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 8; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method8_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method8_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method9_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method9_CallReturn>.Recycle;
    }
    public bool ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 9; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method9_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method9_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = false;
    }
}

internal class MyService_method10_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method10_CallReturn>.Recycle;
    }
    public string ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 10; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method10_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method10_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method11_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method11_CallReturn>.Recycle;
    }
    public float ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 11; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method11_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method11_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method12_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method12_CallReturn>.Recycle;
    }
    public double ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 12; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method12_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method12_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class MyService_method13_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method13_CallReturn>.Recycle;
    }
    public Data ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 13; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method13_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method13_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method14_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method14_CallReturn>.Recycle;
    }
    public Dummy ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 14; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method14_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method14_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method15_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method15_CallReturn>.Recycle;
    }
    public List<Data> ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 15; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method15_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method15_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method16_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method16_CallReturn>.Recycle;
    }
    public Dictionary<uint,Data> ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 16; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method16_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method16_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method17_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method17_CallReturn>.Recycle;
    }
    public HashSet<string> ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 17; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method17_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method17_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = null;
    }
}

internal class MyService_method18_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<MyService_method18_CallReturn>.Recycle;
    }
    public int ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407834107390365; }
    public uint MethodID { get => 18; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        MyServiceCallReturnSerializer.method18_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return MyServiceCallReturnSerializer.method18_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

}

namespace rpc {

internal class Lua_login_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Lua_login_CallReturn>.Recycle;
    }
    public ulong ret;
    public bool NoReturn { get => false; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833380299500; }
    public uint MethodID { get => 1; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaCallReturnSerializer.login_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaCallReturnSerializer.login_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
        ret = 0;
    }
}

internal class Lua_logout_CallReturn : CallReturn, IRecycle {
    uint call_id_ = 0;
    uint service_id_ = 0;
    uint length_ = 0;
    uint error_id_ = (uint)RpcError.rpc_ok;
    public kratos.RecycleFunc RecycleMethod {
        get => kratos.ObjectPool<Lua_logout_CallReturn>.Recycle;
    }
    public bool NoReturn { get => true; }
    public uint CallID { get => call_id_; set => call_id_ = value; }
    public ulong ServiceUUID { get => 5871407833380299500; }
    public uint MethodID { get => 2; }
    public uint ServiceID { get => service_id_; set => service_id_ = value; }
    public uint Length { get => length_; set => length_ = value; }
    public uint ErrorID { get => error_id_; set => error_id_ = value; }
    public void deserialize(GrowingRingBuffer<byte> rb, uint length) {
        LuaCallReturnSerializer.logout_deserialize(this, rb, length);
    }
    public byte[] serialize() {
        return LuaCallReturnSerializer.logout_serialize(this);
    }
    public void OnReset() {
        call_id_ = 0;
        service_id_ = 0;
        length_ = 0;
        error_id_ = 0;
    }
}

}

namespace rpc {

public abstract class ServiceProxy {
    public abstract void method1(Data arg1,string arg2);
    public abstract Task<string> method2(sbyte arg1,HashSet<string> arg2,ulong arg3, long timeout = 5000);
    public abstract Task<Dictionary<long,Dummy>> method3(sbyte arg1,HashSet<string> arg2,Dictionary<long,string> arg3,ulong arg4, long timeout = 5000);
    public abstract Task method4(long timeout = 5000);
    public abstract Task method5(long timeout = 5000);
}

public partial class Proxy {
    public static ServiceProxy GetService(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new ServiceProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

public abstract class ServiceDynamicProxy {
    public abstract void method1(Data arg1,string arg2);
    public abstract Task<string> method2(sbyte arg1,HashSet<string> arg2,ulong arg3, long timeout = 2000);
    public abstract Task<Dictionary<long,Dummy>> method3(sbyte arg1,HashSet<string> arg2,Dictionary<long,string> arg3,ulong arg4, long timeout = 5000);
    public abstract Task method4(long timeout = 5000);
    public abstract Task method5(long timeout = 5000);
}

public partial class Proxy {
    public static ServiceDynamicProxy GetServiceDynamic(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new ServiceDynamicProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

public abstract class LoginProxy {
    public abstract Task<ulong> login(string arg1,string arg2, long timeout = 5000);
    public abstract Task logout(ulong arg1, long timeout = 5000);
}

public partial class Proxy {
    public static LoginProxy GetLogin(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new LoginProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

public abstract class LuaLoginProxy {
    public abstract Task<ulong> login(string arg1,string arg2, long timeout = 5000);
    public abstract Task logout(ulong arg1, long timeout = 5000);
    public abstract void oneway_method();
    public abstract Task normal_method(long timeout = 5000);
}

public partial class Proxy {
    public static LuaLoginProxy GetLuaLogin(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new LuaLoginProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

public abstract class MyServiceProxy {
    public abstract Task<sbyte> method1(long timeout = 5000);
    public abstract Task<short> method2(long timeout = 5000);
    public abstract Task<int> method3(long timeout = 5000);
    public abstract Task<long> method4(long timeout = 5000);
    public abstract Task<byte> method5(long timeout = 5000);
    public abstract Task<ushort> method6(long timeout = 5000);
    public abstract Task<uint> method7(long timeout = 5000);
    public abstract Task<ulong> method8(long timeout = 5000);
    public abstract Task<bool> method9(long timeout = 5000);
    public abstract Task<string> method10(long timeout = 5000);
    public abstract Task<float> method11(long timeout = 5000);
    public abstract Task<double> method12(long timeout = 5000);
    public abstract Task<Data> method13(long timeout = 5000);
    public abstract Task<Dummy> method14(long timeout = 5000);
    public abstract Task<List<Data>> method15(long timeout = 5000);
    public abstract Task<Dictionary<uint,Data>> method16(long timeout = 5000);
    public abstract Task<HashSet<string>> method17(long timeout = 5000);
    public abstract Task<int> method18(sbyte arg1,short arg2,int arg3,long arg4,byte arg5,ushort arg6,uint arg7,ulong arg8,bool arg9,string arg10,float arg11,double arg12,Data arg13,List<Data> arg14,List<string> arg15,Dictionary<uint,Data> arg16,HashSet<string> arg17, long timeout = 5000);
}

public partial class Proxy {
    public static MyServiceProxy GetMyService(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new MyServiceProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

public abstract class LuaProxy {
    public abstract Task<ulong> login(string arg1,string arg2, long timeout = 5000);
    public abstract Task logout(ulong arg1, long timeout = 5000);
}

public partial class Proxy {
    public static LuaProxy GetLua(bool debug_loop = false) {
        if (!Singleton<Rpc>.Instance.IsConnected && !debug_loop) { return null; }
        return new LuaProxyImpl(Singleton<Rpc>.Instance.Client, debug_loop);
    }
}

}

namespace rpc {

internal class ServiceProxyImpl : ServiceProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public ServiceProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override void method1(Data arg1,string arg2) {
        var param = ObjectPool<Service_method1_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.CallID = CallIDGenerator.generate();
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
        }
    }
    public override async Task<string> method2(sbyte arg1,HashSet<string> arg2,ulong arg3, long timeout = 5000) {
        var param = ObjectPool<Service_method2_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.arg3 = arg3;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<string>>.Get();
        awaiter.ServiceUUID = 5871407834219999997;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<Dictionary<long,Dummy>> method3(sbyte arg1,HashSet<string> arg2,Dictionary<long,string> arg3,ulong arg4, long timeout = 5000) {
        var param = ObjectPool<Service_method3_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.arg3 = arg3;
        param.arg4 = arg4;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<Dictionary<long,Dummy>>>.Get();
        awaiter.ServiceUUID = 5871407834219999997;
        awaiter.MethodID = 3;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task method4(long timeout = 5000) {
        var param = ObjectPool<Service_method4_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407834219999997;
        awaiter.MethodID = 4;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
    public override async Task method5(long timeout = 5000) {
        var param = ObjectPool<Service_method5_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407834219999997;
        awaiter.MethodID = 5;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
}

}

namespace rpc {

internal class ServiceDynamicProxyImpl : ServiceDynamicProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public ServiceDynamicProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override void method1(Data arg1,string arg2) {
        var param = ObjectPool<ServiceDynamic_method1_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.CallID = CallIDGenerator.generate();
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
        }
    }
    public override async Task<string> method2(sbyte arg1,HashSet<string> arg2,ulong arg3, long timeout = 2000) {
        var param = ObjectPool<ServiceDynamic_method2_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.arg3 = arg3;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<string>>.Get();
        awaiter.ServiceUUID = 5871407834711899994;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<Dictionary<long,Dummy>> method3(sbyte arg1,HashSet<string> arg2,Dictionary<long,string> arg3,ulong arg4, long timeout = 5000) {
        var param = ObjectPool<ServiceDynamic_method3_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.arg3 = arg3;
        param.arg4 = arg4;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<Dictionary<long,Dummy>>>.Get();
        awaiter.ServiceUUID = 5871407834711899994;
        awaiter.MethodID = 3;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task method4(long timeout = 5000) {
        var param = ObjectPool<ServiceDynamic_method4_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407834711899994;
        awaiter.MethodID = 4;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
    public override async Task method5(long timeout = 5000) {
        var param = ObjectPool<ServiceDynamic_method5_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407834711899994;
        awaiter.MethodID = 5;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
}

}

namespace rpc {

internal class LoginProxyImpl : LoginProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public LoginProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override async Task<ulong> login(string arg1,string arg2, long timeout = 5000) {
        var param = ObjectPool<Login_login_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<ulong>>.Get();
        awaiter.ServiceUUID = 5871407834537456905;
        awaiter.MethodID = 1;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task logout(ulong arg1, long timeout = 5000) {
        var param = ObjectPool<Login_logout_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407834537456905;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
}

}

namespace rpc {

internal class LuaLoginProxyImpl : LuaLoginProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public LuaLoginProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override async Task<ulong> login(string arg1,string arg2, long timeout = 5000) {
        var param = ObjectPool<LuaLogin_login_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<ulong>>.Get();
        awaiter.ServiceUUID = 5871407833815435533;
        awaiter.MethodID = 1;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task logout(ulong arg1, long timeout = 5000) {
        var param = ObjectPool<LuaLogin_logout_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407833815435533;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
    public override void oneway_method() {
        var param = ObjectPool<LuaLogin_oneway_method_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
        }
    }
    public override async Task normal_method(long timeout = 5000) {
        var param = ObjectPool<LuaLogin_normal_method_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407833815435533;
        awaiter.MethodID = 4;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
}

}

namespace rpc {

internal class MyServiceProxyImpl : MyServiceProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public MyServiceProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override async Task<sbyte> method1(long timeout = 5000) {
        var param = ObjectPool<MyService_method1_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<sbyte>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 1;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<short> method2(long timeout = 5000) {
        var param = ObjectPool<MyService_method2_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<short>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<int> method3(long timeout = 5000) {
        var param = ObjectPool<MyService_method3_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<int>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 3;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<long> method4(long timeout = 5000) {
        var param = ObjectPool<MyService_method4_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<long>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 4;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<byte> method5(long timeout = 5000) {
        var param = ObjectPool<MyService_method5_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<byte>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 5;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<ushort> method6(long timeout = 5000) {
        var param = ObjectPool<MyService_method6_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<ushort>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 6;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<uint> method7(long timeout = 5000) {
        var param = ObjectPool<MyService_method7_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<uint>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 7;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<ulong> method8(long timeout = 5000) {
        var param = ObjectPool<MyService_method8_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<ulong>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 8;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<bool> method9(long timeout = 5000) {
        var param = ObjectPool<MyService_method9_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<bool>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 9;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<string> method10(long timeout = 5000) {
        var param = ObjectPool<MyService_method10_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<string>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 10;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<float> method11(long timeout = 5000) {
        var param = ObjectPool<MyService_method11_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<float>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 11;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<double> method12(long timeout = 5000) {
        var param = ObjectPool<MyService_method12_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<double>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 12;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<Data> method13(long timeout = 5000) {
        var param = ObjectPool<MyService_method13_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<Data>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 13;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<Dummy> method14(long timeout = 5000) {
        var param = ObjectPool<MyService_method14_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<Dummy>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 14;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<List<Data>> method15(long timeout = 5000) {
        var param = ObjectPool<MyService_method15_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<List<Data>>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 15;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<Dictionary<uint,Data>> method16(long timeout = 5000) {
        var param = ObjectPool<MyService_method16_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<Dictionary<uint,Data>>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 16;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<HashSet<string>> method17(long timeout = 5000) {
        var param = ObjectPool<MyService_method17_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<HashSet<string>>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 17;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task<int> method18(sbyte arg1,short arg2,int arg3,long arg4,byte arg5,ushort arg6,uint arg7,ulong arg8,bool arg9,string arg10,float arg11,double arg12,Data arg13,List<Data> arg14,List<string> arg15,Dictionary<uint,Data> arg16,HashSet<string> arg17, long timeout = 5000) {
        var param = ObjectPool<MyService_method18_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.arg3 = arg3;
        param.arg4 = arg4;
        param.arg5 = arg5;
        param.arg6 = arg6;
        param.arg7 = arg7;
        param.arg8 = arg8;
        param.arg9 = arg9;
        param.arg10 = arg10;
        param.arg11 = arg11;
        param.arg12 = arg12;
        param.arg13 = arg13;
        param.arg14 = arg14;
        param.arg15 = arg15;
        param.arg16 = arg16;
        param.arg17 = arg17;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<int>>.Get();
        awaiter.ServiceUUID = 5871407834107390365;
        awaiter.MethodID = 18;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
}

}

namespace rpc {

internal class LuaProxyImpl : LuaProxy {
    IClient client_ = null;
    bool debug_loop_ = false;
    public LuaProxyImpl(IClient client, bool debug_loop = false) {
        debug_loop_ = debug_loop;
        client_ = client;
    }
    public override async Task<ulong> login(string arg1,string arg2, long timeout = 5000) {
        var param = ObjectPool<Lua_login_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.arg2 = arg2;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter<ulong>>.Get();
        awaiter.ServiceUUID = 5871407833380299500;
        awaiter.MethodID = 1;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
            return awaiter.GetResult();
        } else {
            client_.Send(param);
            return await awaiter;
        }
    }
    public override async Task logout(ulong arg1, long timeout = 5000) {
        var param = ObjectPool<Lua_logout_CallParam>.Get();
        param.IsDebugLoop = debug_loop_;
        param.arg1 = arg1;
        param.CallID = CallIDGenerator.generate();
        var awaiter = ObjectPool<ProxyCallAwaiter>.Get();
        awaiter.ServiceUUID = 5871407833380299500;
        awaiter.MethodID = 2;
        awaiter.CallID = param.CallID;
        awaiter.Timeout = timeout;
        awaiter.DebugLoop = debug_loop_;
        if (debug_loop_) {
            Singleton<Rpc>.Instance.OnMessage(param);
        } else {
            client_.Send(param);
            await awaiter;
        }
    }
}

}

namespace rpc {

    public static class exampleRegister {
        public static void DoRegisterMethodParam() {
            Singleton<ParamRegister>.Instance.Register.Add(5871407834219999997, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407834219999997].Add(1, Service_method1_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834219999997].Add(2, Service_method2_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834219999997].Add(3, Service_method3_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834219999997].Add(4, Service_method4_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834219999997].Add(5, Service_method5_delegate.NewParam);

            Singleton<ParamRegister>.Instance.Register.Add(5871407834711899994, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407834711899994].Add(1, ServiceDynamic_method1_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834711899994].Add(2, ServiceDynamic_method2_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834711899994].Add(3, ServiceDynamic_method3_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834711899994].Add(4, ServiceDynamic_method4_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834711899994].Add(5, ServiceDynamic_method5_delegate.NewParam);

            Singleton<ParamRegister>.Instance.Register.Add(5871407834537456905, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407834537456905].Add(1, Login_login_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834537456905].Add(2, Login_logout_delegate.NewParam);

            Singleton<ParamRegister>.Instance.Register.Add(5871407833815435533, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407833815435533].Add(1, LuaLogin_login_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407833815435533].Add(2, LuaLogin_logout_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407833815435533].Add(3, LuaLogin_oneway_method_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407833815435533].Add(4, LuaLogin_normal_method_delegate.NewParam);

            Singleton<ParamRegister>.Instance.Register.Add(5871407834107390365, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(1, MyService_method1_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(2, MyService_method2_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(3, MyService_method3_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(4, MyService_method4_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(5, MyService_method5_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(6, MyService_method6_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(7, MyService_method7_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(8, MyService_method8_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(9, MyService_method9_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(10, MyService_method10_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(11, MyService_method11_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(12, MyService_method12_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(13, MyService_method13_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(14, MyService_method14_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(15, MyService_method15_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(16, MyService_method16_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(17, MyService_method17_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407834107390365].Add(18, MyService_method18_delegate.NewParam);

            Singleton<ParamRegister>.Instance.Register.Add(5871407833380299500, new Dictionary<uint, NewParamByUUID>());
            Singleton<ParamRegister>.Instance.Register[5871407833380299500].Add(1, Lua_login_delegate.NewParam);
            Singleton<ParamRegister>.Instance.Register[5871407833380299500].Add(2, Lua_logout_delegate.NewParam);

        }
        public static void DoRegisterMethodReturn() {
            Singleton<ReturnRegister>.Instance.Register.Add(5871407834219999997, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407834219999997].Add(1, Service_method1_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834219999997].Add(2, Service_method2_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834219999997].Add(3, Service_method3_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834219999997].Add(4, Service_method4_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834219999997].Add(5, Service_method5_delegate.NewReturn);

            Singleton<ReturnRegister>.Instance.Register.Add(5871407834711899994, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407834711899994].Add(1, ServiceDynamic_method1_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834711899994].Add(2, ServiceDynamic_method2_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834711899994].Add(3, ServiceDynamic_method3_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834711899994].Add(4, ServiceDynamic_method4_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834711899994].Add(5, ServiceDynamic_method5_delegate.NewReturn);

            Singleton<ReturnRegister>.Instance.Register.Add(5871407834537456905, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407834537456905].Add(1, Login_login_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834537456905].Add(2, Login_logout_delegate.NewReturn);

            Singleton<ReturnRegister>.Instance.Register.Add(5871407833815435533, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407833815435533].Add(1, LuaLogin_login_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407833815435533].Add(2, LuaLogin_logout_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407833815435533].Add(3, LuaLogin_oneway_method_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407833815435533].Add(4, LuaLogin_normal_method_delegate.NewReturn);

            Singleton<ReturnRegister>.Instance.Register.Add(5871407834107390365, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(1, MyService_method1_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(2, MyService_method2_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(3, MyService_method3_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(4, MyService_method4_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(5, MyService_method5_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(6, MyService_method6_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(7, MyService_method7_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(8, MyService_method8_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(9, MyService_method9_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(10, MyService_method10_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(11, MyService_method11_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(12, MyService_method12_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(13, MyService_method13_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(14, MyService_method14_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(15, MyService_method15_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(16, MyService_method16_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(17, MyService_method17_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407834107390365].Add(18, MyService_method18_delegate.NewReturn);

            Singleton<ReturnRegister>.Instance.Register.Add(5871407833380299500, new Dictionary<uint, NewReturnByUUID>());
            Singleton<ReturnRegister>.Instance.Register[5871407833380299500].Add(1, Lua_login_delegate.NewReturn);
            Singleton<ReturnRegister>.Instance.Register[5871407833380299500].Add(2, Lua_logout_delegate.NewReturn);

        }
    }
}

namespace rpc {

internal static class exampleCaller {
    internal static void Service_method1(IClient client, CallParam param, IService service) {
        ServiceImpl impl = (ServiceImpl)service;
        Service_method1_CallParam param_impl = (Service_method1_CallParam)param;
        try {
            impl.method1(param_impl.arg1, param_impl.arg2);
        } catch (Exception) {
        }
    }
    internal static void Service_method2(IClient client, CallParam param, IService service) {
        ServiceImpl impl = (ServiceImpl)service;
        Service_method2_CallParam param_impl = (Service_method2_CallParam)param;
        Service_method2_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<Service_method2_CallReturn>.Get();
            var retval = impl.method2(param_impl.arg1, param_impl.arg2, param_impl.arg3);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<string>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Service_method3(IClient client, CallParam param, IService service) {
        ServiceImpl impl = (ServiceImpl)service;
        Service_method3_CallParam param_impl = (Service_method3_CallParam)param;
        Service_method3_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<Service_method3_CallReturn>.Get();
            var retval = impl.method3(param_impl.arg1, param_impl.arg2, param_impl.arg3, param_impl.arg4);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<Dictionary<long,Dummy>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Service_method4(IClient client, CallParam param, IService service) {
        ServiceImpl impl = (ServiceImpl)service;
        Service_method4_CallParam param_impl = (Service_method4_CallParam)param;
        Service_method4_CallReturn ret = null;
        try {
            impl.method4();
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Service_method5(IClient client, CallParam param, IService service) {
        ServiceImpl impl = (ServiceImpl)service;
        Service_method5_CallParam param_impl = (Service_method5_CallParam)param;
        Service_method5_CallReturn ret = null;
        try {
            impl.method5();
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void ServiceDynamic_method1(IClient client, CallParam param, IService service) {
        ServiceDynamicImpl impl = (ServiceDynamicImpl)service;
        ServiceDynamic_method1_CallParam param_impl = (ServiceDynamic_method1_CallParam)param;
        try {
            impl.method1(param_impl.arg1, param_impl.arg2);
        } catch (Exception) {
        }
    }
    internal static void ServiceDynamic_method2(IClient client, CallParam param, IService service) {
        ServiceDynamicImpl impl = (ServiceDynamicImpl)service;
        ServiceDynamic_method2_CallParam param_impl = (ServiceDynamic_method2_CallParam)param;
        ServiceDynamic_method2_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<ServiceDynamic_method2_CallReturn>.Get();
            var retval = impl.method2(param_impl.arg1, param_impl.arg2, param_impl.arg3);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<string>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void ServiceDynamic_method3(IClient client, CallParam param, IService service) {
        ServiceDynamicImpl impl = (ServiceDynamicImpl)service;
        ServiceDynamic_method3_CallParam param_impl = (ServiceDynamic_method3_CallParam)param;
        ServiceDynamic_method3_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<ServiceDynamic_method3_CallReturn>.Get();
            var retval = impl.method3(param_impl.arg1, param_impl.arg2, param_impl.arg3, param_impl.arg4);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<Dictionary<long,Dummy>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void ServiceDynamic_method4(IClient client, CallParam param, IService service) {
        ServiceDynamicImpl impl = (ServiceDynamicImpl)service;
        ServiceDynamic_method4_CallParam param_impl = (ServiceDynamic_method4_CallParam)param;
        ServiceDynamic_method4_CallReturn ret = null;
        try {
            impl.method4();
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void ServiceDynamic_method5(IClient client, CallParam param, IService service) {
        ServiceDynamicImpl impl = (ServiceDynamicImpl)service;
        ServiceDynamic_method5_CallParam param_impl = (ServiceDynamic_method5_CallParam)param;
        ServiceDynamic_method5_CallReturn ret = null;
        try {
            impl.method5();
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Login_login(IClient client, CallParam param, IService service) {
        LoginImpl impl = (LoginImpl)service;
        Login_login_CallParam param_impl = (Login_login_CallParam)param;
        Login_login_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<Login_login_CallReturn>.Get();
            var retval = impl.login(param_impl.arg1, param_impl.arg2);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Login_logout(IClient client, CallParam param, IService service) {
        LoginImpl impl = (LoginImpl)service;
        Login_logout_CallParam param_impl = (Login_logout_CallParam)param;
        Login_logout_CallReturn ret = null;
        try {
            impl.logout(param_impl.arg1);
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void LuaLogin_login(IClient client, CallParam param, IService service) {
        LuaLoginImpl impl = (LuaLoginImpl)service;
        LuaLogin_login_CallParam param_impl = (LuaLogin_login_CallParam)param;
        LuaLogin_login_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<LuaLogin_login_CallReturn>.Get();
            var retval = impl.login(param_impl.arg1, param_impl.arg2);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void LuaLogin_logout(IClient client, CallParam param, IService service) {
        LuaLoginImpl impl = (LuaLoginImpl)service;
        LuaLogin_logout_CallParam param_impl = (LuaLogin_logout_CallParam)param;
        LuaLogin_logout_CallReturn ret = null;
        try {
            impl.logout(param_impl.arg1);
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void LuaLogin_oneway_method(IClient client, CallParam param, IService service) {
        LuaLoginImpl impl = (LuaLoginImpl)service;
        LuaLogin_oneway_method_CallParam param_impl = (LuaLogin_oneway_method_CallParam)param;
        try {
            impl.oneway_method();
        } catch (Exception) {
        }
    }
    internal static void LuaLogin_normal_method(IClient client, CallParam param, IService service) {
        LuaLoginImpl impl = (LuaLoginImpl)service;
        LuaLogin_normal_method_CallParam param_impl = (LuaLogin_normal_method_CallParam)param;
        LuaLogin_normal_method_CallReturn ret = null;
        try {
            impl.normal_method();
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Lua_login(IClient client, CallParam param, IService service) {
        LuaImpl impl = (LuaImpl)service;
        Lua_login_CallParam param_impl = (Lua_login_CallParam)param;
        Lua_login_CallReturn ret = null;
        try {
            ret = kratos.ObjectPool<Lua_login_CallReturn>.Get();
            var retval = impl.login(param_impl.arg1, param_impl.arg2);
            ret.ret = retval;
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            var waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
            waiter.SetResult(ret.ret);
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    internal static void Lua_logout(IClient client, CallParam param, IService service) {
        LuaImpl impl = (LuaImpl)service;
        Lua_logout_CallParam param_impl = (Lua_logout_CallParam)param;
        Lua_logout_CallReturn ret = null;
        try {
            impl.logout(param_impl.arg1);
            ret.ErrorID = (uint)RpcError.rpc_ok;
        } catch (Exception) {
            ret.ErrorID = (uint)RpcError.rpc_exception;
        }
        ret.CallID = param.CallID;
        ret.ServiceID = service.ID;
        if (param.IsDebugLoop) {
            Singleton<Rpc>.Instance.OnMessage(ret);
        } else {
            client.Send(ret);
        }
    }
    public static void DoCallerRegister() {
        Singleton<ServiceManager>.Instance.RegisterService(5871407834219999997, new ServiceImpl());
        Singleton<ServiceCaller>.Instance.Add(5871407834219999997, 1, Service_method1);
        Singleton<ServiceCaller>.Instance.Add(5871407834219999997, 2, Service_method2);
        Singleton<ServiceCaller>.Instance.Add(5871407834219999997, 3, Service_method3);
        Singleton<ServiceCaller>.Instance.Add(5871407834219999997, 4, Service_method4);
        Singleton<ServiceCaller>.Instance.Add(5871407834219999997, 5, Service_method5);
        Singleton<ServiceManager>.Instance.RegisterService(5871407834711899994, new ServiceDynamicImpl());
        Singleton<ServiceCaller>.Instance.Add(5871407834711899994, 1, ServiceDynamic_method1);
        Singleton<ServiceCaller>.Instance.Add(5871407834711899994, 2, ServiceDynamic_method2);
        Singleton<ServiceCaller>.Instance.Add(5871407834711899994, 3, ServiceDynamic_method3);
        Singleton<ServiceCaller>.Instance.Add(5871407834711899994, 4, ServiceDynamic_method4);
        Singleton<ServiceCaller>.Instance.Add(5871407834711899994, 5, ServiceDynamic_method5);
        Singleton<ServiceManager>.Instance.RegisterService(5871407834537456905, new LoginImpl());
        Singleton<ServiceCaller>.Instance.Add(5871407834537456905, 1, Login_login);
        Singleton<ServiceCaller>.Instance.Add(5871407834537456905, 2, Login_logout);
        Singleton<ServiceManager>.Instance.RegisterService(5871407833815435533, new LuaLoginImpl());
        Singleton<ServiceCaller>.Instance.Add(5871407833815435533, 1, LuaLogin_login);
        Singleton<ServiceCaller>.Instance.Add(5871407833815435533, 2, LuaLogin_logout);
        Singleton<ServiceCaller>.Instance.Add(5871407833815435533, 3, LuaLogin_oneway_method);
        Singleton<ServiceCaller>.Instance.Add(5871407833815435533, 4, LuaLogin_normal_method);
        Singleton<ServiceManager>.Instance.RegisterService(5871407833380299500, new LuaImpl());
        Singleton<ServiceCaller>.Instance.Add(5871407833380299500, 1, Lua_login);
        Singleton<ServiceCaller>.Instance.Add(5871407833380299500, 2, Lua_logout);
    }
}
}

#endregion Machine generated code

