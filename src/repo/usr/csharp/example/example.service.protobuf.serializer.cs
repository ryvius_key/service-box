// Machine generated code

#region Machine generated code

using kratos;
using RingBuffer;
using Google.Protobuf;
using Example;
using System.Collections.Generic;

namespace rpc {

internal static class PbParamClear {
        internal static void Service_method1_ParamClear(Service_method1_args args) {
            if (args == null) { return; }
            args.Arg1 = null;
            args.Arg2 = "";
        }
        internal static void Service_method2_ParamClear(Service_method2_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
            args.Arg2.Clear();
            args.Arg3 = 0;
        }
        internal static void Service_method3_ParamClear(Service_method3_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
            args.Arg2.Clear();
            args.Arg3.Clear();
            args.Arg4 = 0;
        }
        internal static void ServiceDynamic_method1_ParamClear(ServiceDynamic_method1_args args) {
            if (args == null) { return; }
            args.Arg1 = null;
            args.Arg2 = "";
        }
        internal static void ServiceDynamic_method2_ParamClear(ServiceDynamic_method2_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
            args.Arg2.Clear();
            args.Arg3 = 0;
        }
        internal static void ServiceDynamic_method3_ParamClear(ServiceDynamic_method3_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
            args.Arg2.Clear();
            args.Arg3.Clear();
            args.Arg4 = 0;
        }
        internal static void Login_login_ParamClear(Login_login_args args) {
            if (args == null) { return; }
            args.Arg1 = "";
            args.Arg2 = "";
        }
        internal static void Login_logout_ParamClear(Login_logout_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
        }
        internal static void LuaLogin_login_ParamClear(LuaLogin_login_args args) {
            if (args == null) { return; }
            args.Arg1 = "";
            args.Arg2 = "";
        }
        internal static void LuaLogin_logout_ParamClear(LuaLogin_logout_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
        }
        internal static void MyService_method18_ParamClear(MyService_method18_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
            args.Arg2 = 0;
            args.Arg3 = 0;
            args.Arg4 = 0;
            args.Arg5 = 0;
            args.Arg6 = 0;
            args.Arg7 = 0;
            args.Arg8 = 0;
            args.Arg9 = false;
            args.Arg10 = "";
            args.Arg11 = 0;
            args.Arg12 = 0;
            args.Arg13 = null;
            args.Arg14.Clear();
            args.Arg15.Clear();
            args.Arg16.Clear();
            args.Arg17.Clear();
        }
        internal static void Lua_login_ParamClear(Lua_login_args args) {
            if (args == null) { return; }
            args.Arg1 = "";
            args.Arg2 = "";
        }
        internal static void Lua_logout_ParamClear(Lua_logout_args args) {
            if (args == null) { return; }
            args.Arg1 = 0;
        }
}

internal static class PbRetClear {
        internal static void Service_method2_RetClear(Service_method2_ret ret) {
            if (ret == null) { return; }
            ret.Ret = "";
        }
        internal static void Service_method3_RetClear(Service_method3_ret ret) {
            if (ret == null) { return; }
            ret.Ret.Clear();
        }
        internal static void ServiceDynamic_method2_RetClear(ServiceDynamic_method2_ret ret) {
            if (ret == null) { return; }
            ret.Ret = "";
        }
        internal static void ServiceDynamic_method3_RetClear(ServiceDynamic_method3_ret ret) {
            if (ret == null) { return; }
            ret.Ret.Clear();
        }
        internal static void Login_login_RetClear(Login_login_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void LuaLogin_login_RetClear(LuaLogin_login_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method1_RetClear(MyService_method1_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method2_RetClear(MyService_method2_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method3_RetClear(MyService_method3_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method4_RetClear(MyService_method4_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method5_RetClear(MyService_method5_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method6_RetClear(MyService_method6_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method7_RetClear(MyService_method7_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method8_RetClear(MyService_method8_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method9_RetClear(MyService_method9_ret ret) {
            if (ret == null) { return; }
            ret.Ret = false;
        }
        internal static void MyService_method10_RetClear(MyService_method10_ret ret) {
            if (ret == null) { return; }
            ret.Ret = "";
        }
        internal static void MyService_method11_RetClear(MyService_method11_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method12_RetClear(MyService_method12_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void MyService_method13_RetClear(MyService_method13_ret ret) {
            if (ret == null) { return; }
            ret.Ret = null;
        }
        internal static void MyService_method14_RetClear(MyService_method14_ret ret) {
            if (ret == null) { return; }
            ret.Ret = null;
        }
        internal static void MyService_method15_RetClear(MyService_method15_ret ret) {
            if (ret == null) { return; }
            ret.Ret.Clear();
        }
        internal static void MyService_method16_RetClear(MyService_method16_ret ret) {
            if (ret == null) { return; }
            ret.Ret.Clear();
        }
        internal static void MyService_method17_RetClear(MyService_method17_ret ret) {
            if (ret == null) { return; }
            ret.Ret.Clear();
        }
        internal static void MyService_method18_RetClear(MyService_method18_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
        internal static void Lua_login_RetClear(Lua_login_ret ret) {
            if (ret == null) { return; }
            ret.Ret = 0;
        }
}

}

namespace rpc {

internal class ServiceCallParamSerializer {
    private static Google.Protobuf.MessageParser<Service_method1_args> Service_method1_args_parser = new Google.Protobuf.MessageParser<Service_method1_args>(()=>ObjectPoolPlain<Service_method1_args>.Get());
    private static Google.Protobuf.MessageParser<Service_method2_args> Service_method2_args_parser = new Google.Protobuf.MessageParser<Service_method2_args>(()=>ObjectPoolPlain<Service_method2_args>.Get());
    private static Google.Protobuf.MessageParser<Service_method3_args> Service_method3_args_parser = new Google.Protobuf.MessageParser<Service_method3_args>(()=>ObjectPoolPlain<Service_method3_args>.Get());
    public static void method1_deserialize(Service_method1_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Service_method1_args args = Service_method1_args_parser.ParseFrom(buffer, 0, (int)length);
            ((DataImpl)(param.arg1)).PBObject = args.Arg1;
            param.arg2 = args.Arg2;
            PbParamClear.Service_method1_ParamClear(args);
            ObjectPoolPlain<Service_method1_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method1_serialize(Service_method1_CallParam param) {
        if (param.NoParam) { return null; }
        Service_method1_args args = kratos.ObjectPoolPlain<Service_method1_args>.Get();
        args.Arg1 = ((DataImpl)(param.arg1)).PBObject;
        args.Arg2 = param.arg2;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Service_method1_ParamClear(args);
        kratos.ObjectPoolPlain<Service_method1_args>.Recycle(args);
        return buffer;
    }
    public static void method2_deserialize(Service_method2_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Service_method2_args args = Service_method2_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (sbyte)args.Arg1;
            foreach (var k in args.Arg2) {
                param.arg2.Add(k);
            }
            param.arg3 = (ulong)args.Arg3;
            PbParamClear.Service_method2_ParamClear(args);
            ObjectPoolPlain<Service_method2_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method2_serialize(Service_method2_CallParam param) {
        if (param.NoParam) { return null; }
        Service_method2_args args = kratos.ObjectPoolPlain<Service_method2_args>.Get();
        args.Arg1 = param.arg1;
        foreach (var k in param.arg2) {
            args.Arg2.Add(k);
        }
        args.Arg3 = param.arg3;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Service_method2_ParamClear(args);
        kratos.ObjectPoolPlain<Service_method2_args>.Recycle(args);
        return buffer;
    }
    public static void method3_deserialize(Service_method3_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Service_method3_args args = Service_method3_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (sbyte)args.Arg1;
            foreach (var k in args.Arg2) {
                param.arg2.Add(k);
            }
            foreach (var k in args.Arg3) {
                param.arg3.Add(k.Key, k.Value);
            }
            param.arg4 = (ulong)args.Arg4;
            PbParamClear.Service_method3_ParamClear(args);
            ObjectPoolPlain<Service_method3_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method3_serialize(Service_method3_CallParam param) {
        if (param.NoParam) { return null; }
        Service_method3_args args = kratos.ObjectPoolPlain<Service_method3_args>.Get();
        args.Arg1 = param.arg1;
        foreach (var k in param.arg2) {
            args.Arg2.Add(k);
        }
        foreach (var k in param.arg3) {
            args.Arg3.Add(k.Key, k.Value);
        }
        args.Arg4 = param.arg4;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Service_method3_ParamClear(args);
        kratos.ObjectPoolPlain<Service_method3_args>.Recycle(args);
        return buffer;
    }
    public static void method4_deserialize(Service_method4_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method4_serialize(Service_method4_CallParam param) {
        return null;
    }
    public static void method5_deserialize(Service_method5_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method5_serialize(Service_method5_CallParam param) {
        return null;
    }
}

}

namespace rpc {

internal class ServiceDynamicCallParamSerializer {
    private static Google.Protobuf.MessageParser<ServiceDynamic_method1_args> ServiceDynamic_method1_args_parser = new Google.Protobuf.MessageParser<ServiceDynamic_method1_args>(()=>ObjectPoolPlain<ServiceDynamic_method1_args>.Get());
    private static Google.Protobuf.MessageParser<ServiceDynamic_method2_args> ServiceDynamic_method2_args_parser = new Google.Protobuf.MessageParser<ServiceDynamic_method2_args>(()=>ObjectPoolPlain<ServiceDynamic_method2_args>.Get());
    private static Google.Protobuf.MessageParser<ServiceDynamic_method3_args> ServiceDynamic_method3_args_parser = new Google.Protobuf.MessageParser<ServiceDynamic_method3_args>(()=>ObjectPoolPlain<ServiceDynamic_method3_args>.Get());
    public static void method1_deserialize(ServiceDynamic_method1_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ServiceDynamic_method1_args args = ServiceDynamic_method1_args_parser.ParseFrom(buffer, 0, (int)length);
            ((DataImpl)(param.arg1)).PBObject = args.Arg1;
            param.arg2 = args.Arg2;
            PbParamClear.ServiceDynamic_method1_ParamClear(args);
            ObjectPoolPlain<ServiceDynamic_method1_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method1_serialize(ServiceDynamic_method1_CallParam param) {
        if (param.NoParam) { return null; }
        ServiceDynamic_method1_args args = kratos.ObjectPoolPlain<ServiceDynamic_method1_args>.Get();
        args.Arg1 = ((DataImpl)(param.arg1)).PBObject;
        args.Arg2 = param.arg2;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.ServiceDynamic_method1_ParamClear(args);
        kratos.ObjectPoolPlain<ServiceDynamic_method1_args>.Recycle(args);
        return buffer;
    }
    public static void method2_deserialize(ServiceDynamic_method2_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ServiceDynamic_method2_args args = ServiceDynamic_method2_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (sbyte)args.Arg1;
            foreach (var k in args.Arg2) {
                param.arg2.Add(k);
            }
            param.arg3 = (ulong)args.Arg3;
            PbParamClear.ServiceDynamic_method2_ParamClear(args);
            ObjectPoolPlain<ServiceDynamic_method2_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method2_serialize(ServiceDynamic_method2_CallParam param) {
        if (param.NoParam) { return null; }
        ServiceDynamic_method2_args args = kratos.ObjectPoolPlain<ServiceDynamic_method2_args>.Get();
        args.Arg1 = param.arg1;
        foreach (var k in param.arg2) {
            args.Arg2.Add(k);
        }
        args.Arg3 = param.arg3;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.ServiceDynamic_method2_ParamClear(args);
        kratos.ObjectPoolPlain<ServiceDynamic_method2_args>.Recycle(args);
        return buffer;
    }
    public static void method3_deserialize(ServiceDynamic_method3_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ServiceDynamic_method3_args args = ServiceDynamic_method3_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (sbyte)args.Arg1;
            foreach (var k in args.Arg2) {
                param.arg2.Add(k);
            }
            foreach (var k in args.Arg3) {
                param.arg3.Add(k.Key, k.Value);
            }
            param.arg4 = (ulong)args.Arg4;
            PbParamClear.ServiceDynamic_method3_ParamClear(args);
            ObjectPoolPlain<ServiceDynamic_method3_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method3_serialize(ServiceDynamic_method3_CallParam param) {
        if (param.NoParam) { return null; }
        ServiceDynamic_method3_args args = kratos.ObjectPoolPlain<ServiceDynamic_method3_args>.Get();
        args.Arg1 = param.arg1;
        foreach (var k in param.arg2) {
            args.Arg2.Add(k);
        }
        foreach (var k in param.arg3) {
            args.Arg3.Add(k.Key, k.Value);
        }
        args.Arg4 = param.arg4;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.ServiceDynamic_method3_ParamClear(args);
        kratos.ObjectPoolPlain<ServiceDynamic_method3_args>.Recycle(args);
        return buffer;
    }
    public static void method4_deserialize(ServiceDynamic_method4_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method4_serialize(ServiceDynamic_method4_CallParam param) {
        return null;
    }
    public static void method5_deserialize(ServiceDynamic_method5_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method5_serialize(ServiceDynamic_method5_CallParam param) {
        return null;
    }
}

}

namespace rpc {

internal class LoginCallParamSerializer {
    private static Google.Protobuf.MessageParser<Login_login_args> Login_login_args_parser = new Google.Protobuf.MessageParser<Login_login_args>(()=>ObjectPoolPlain<Login_login_args>.Get());
    private static Google.Protobuf.MessageParser<Login_logout_args> Login_logout_args_parser = new Google.Protobuf.MessageParser<Login_logout_args>(()=>ObjectPoolPlain<Login_logout_args>.Get());
    public static void login_deserialize(Login_login_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Login_login_args args = Login_login_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = args.Arg1;
            param.arg2 = args.Arg2;
            PbParamClear.Login_login_ParamClear(args);
            ObjectPoolPlain<Login_login_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] login_serialize(Login_login_CallParam param) {
        if (param.NoParam) { return null; }
        Login_login_args args = kratos.ObjectPoolPlain<Login_login_args>.Get();
        args.Arg1 = param.arg1;
        args.Arg2 = param.arg2;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Login_login_ParamClear(args);
        kratos.ObjectPoolPlain<Login_login_args>.Recycle(args);
        return buffer;
    }
    public static void logout_deserialize(Login_logout_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Login_logout_args args = Login_logout_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (ulong)args.Arg1;
            PbParamClear.Login_logout_ParamClear(args);
            ObjectPoolPlain<Login_logout_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] logout_serialize(Login_logout_CallParam param) {
        if (param.NoParam) { return null; }
        Login_logout_args args = kratos.ObjectPoolPlain<Login_logout_args>.Get();
        args.Arg1 = param.arg1;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Login_logout_ParamClear(args);
        kratos.ObjectPoolPlain<Login_logout_args>.Recycle(args);
        return buffer;
    }
}

}

namespace rpc {

internal class LuaLoginCallParamSerializer {
    private static Google.Protobuf.MessageParser<LuaLogin_login_args> LuaLogin_login_args_parser = new Google.Protobuf.MessageParser<LuaLogin_login_args>(()=>ObjectPoolPlain<LuaLogin_login_args>.Get());
    private static Google.Protobuf.MessageParser<LuaLogin_logout_args> LuaLogin_logout_args_parser = new Google.Protobuf.MessageParser<LuaLogin_logout_args>(()=>ObjectPoolPlain<LuaLogin_logout_args>.Get());
    public static void login_deserialize(LuaLogin_login_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            LuaLogin_login_args args = LuaLogin_login_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = args.Arg1;
            param.arg2 = args.Arg2;
            PbParamClear.LuaLogin_login_ParamClear(args);
            ObjectPoolPlain<LuaLogin_login_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] login_serialize(LuaLogin_login_CallParam param) {
        if (param.NoParam) { return null; }
        LuaLogin_login_args args = kratos.ObjectPoolPlain<LuaLogin_login_args>.Get();
        args.Arg1 = param.arg1;
        args.Arg2 = param.arg2;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.LuaLogin_login_ParamClear(args);
        kratos.ObjectPoolPlain<LuaLogin_login_args>.Recycle(args);
        return buffer;
    }
    public static void logout_deserialize(LuaLogin_logout_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            LuaLogin_logout_args args = LuaLogin_logout_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (ulong)args.Arg1;
            PbParamClear.LuaLogin_logout_ParamClear(args);
            ObjectPoolPlain<LuaLogin_logout_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] logout_serialize(LuaLogin_logout_CallParam param) {
        if (param.NoParam) { return null; }
        LuaLogin_logout_args args = kratos.ObjectPoolPlain<LuaLogin_logout_args>.Get();
        args.Arg1 = param.arg1;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.LuaLogin_logout_ParamClear(args);
        kratos.ObjectPoolPlain<LuaLogin_logout_args>.Recycle(args);
        return buffer;
    }
    public static void oneway_method_deserialize(LuaLogin_oneway_method_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] oneway_method_serialize(LuaLogin_oneway_method_CallParam param) {
        return null;
    }
    public static void normal_method_deserialize(LuaLogin_normal_method_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] normal_method_serialize(LuaLogin_normal_method_CallParam param) {
        return null;
    }
}

}

namespace rpc {

internal class MyServiceCallParamSerializer {
    private static Google.Protobuf.MessageParser<MyService_method18_args> MyService_method18_args_parser = new Google.Protobuf.MessageParser<MyService_method18_args>(()=>ObjectPoolPlain<MyService_method18_args>.Get());
    public static void method1_deserialize(MyService_method1_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method1_serialize(MyService_method1_CallParam param) {
        return null;
    }
    public static void method2_deserialize(MyService_method2_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method2_serialize(MyService_method2_CallParam param) {
        return null;
    }
    public static void method3_deserialize(MyService_method3_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method3_serialize(MyService_method3_CallParam param) {
        return null;
    }
    public static void method4_deserialize(MyService_method4_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method4_serialize(MyService_method4_CallParam param) {
        return null;
    }
    public static void method5_deserialize(MyService_method5_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method5_serialize(MyService_method5_CallParam param) {
        return null;
    }
    public static void method6_deserialize(MyService_method6_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method6_serialize(MyService_method6_CallParam param) {
        return null;
    }
    public static void method7_deserialize(MyService_method7_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method7_serialize(MyService_method7_CallParam param) {
        return null;
    }
    public static void method8_deserialize(MyService_method8_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method8_serialize(MyService_method8_CallParam param) {
        return null;
    }
    public static void method9_deserialize(MyService_method9_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method9_serialize(MyService_method9_CallParam param) {
        return null;
    }
    public static void method10_deserialize(MyService_method10_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method10_serialize(MyService_method10_CallParam param) {
        return null;
    }
    public static void method11_deserialize(MyService_method11_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method11_serialize(MyService_method11_CallParam param) {
        return null;
    }
    public static void method12_deserialize(MyService_method12_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method12_serialize(MyService_method12_CallParam param) {
        return null;
    }
    public static void method13_deserialize(MyService_method13_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method13_serialize(MyService_method13_CallParam param) {
        return null;
    }
    public static void method14_deserialize(MyService_method14_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method14_serialize(MyService_method14_CallParam param) {
        return null;
    }
    public static void method15_deserialize(MyService_method15_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method15_serialize(MyService_method15_CallParam param) {
        return null;
    }
    public static void method16_deserialize(MyService_method16_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method16_serialize(MyService_method16_CallParam param) {
        return null;
    }
    public static void method17_deserialize(MyService_method17_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method17_serialize(MyService_method17_CallParam param) {
        return null;
    }
    public static void method18_deserialize(MyService_method18_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            MyService_method18_args args = MyService_method18_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (sbyte)args.Arg1;
            param.arg2 = (short)args.Arg2;
            param.arg3 = (int)args.Arg3;
            param.arg4 = (long)args.Arg4;
            param.arg5 = (byte)args.Arg5;
            param.arg6 = (ushort)args.Arg6;
            param.arg7 = (uint)args.Arg7;
            param.arg8 = (ulong)args.Arg8;
            param.arg9 = (bool)args.Arg9;
            param.arg10 = args.Arg10;
            param.arg11 = (float)args.Arg11;
            param.arg12 = (double)args.Arg12;
            ((DataImpl)(param.arg13)).PBObject = args.Arg13;
            foreach (var k in args.Arg14) {
                param.arg14.Add(new DataImpl(k));
            }
            foreach (var k in args.Arg15) {
                param.arg15.Add(k);
            }
            foreach (var k in args.Arg16) {
                param.arg16.Add(k.Key, new DataImpl(k.Value));
            }
            foreach (var k in args.Arg17) {
                param.arg17.Add(k);
            }
            PbParamClear.MyService_method18_ParamClear(args);
            ObjectPoolPlain<MyService_method18_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] method18_serialize(MyService_method18_CallParam param) {
        if (param.NoParam) { return null; }
        MyService_method18_args args = kratos.ObjectPoolPlain<MyService_method18_args>.Get();
        args.Arg1 = param.arg1;
        args.Arg2 = param.arg2;
        args.Arg3 = param.arg3;
        args.Arg4 = param.arg4;
        args.Arg5 = param.arg5;
        args.Arg6 = param.arg6;
        args.Arg7 = param.arg7;
        args.Arg8 = param.arg8;
        args.Arg9 = param.arg9;
        args.Arg10 = param.arg10;
        args.Arg11 = param.arg11;
        args.Arg12 = param.arg12;
        args.Arg13 = ((DataImpl)(param.arg13)).PBObject;
        foreach (var k in param.arg14) {
            args.Arg14.Add(((DataImpl)k).PBObject);
        }
        foreach (var k in param.arg15) {
            args.Arg15.Add(k);
        }
        foreach (var k in param.arg16) {
            args.Arg16.Add(k.Key, ((DataImpl)k.Value).PBObject);
        }
        foreach (var k in param.arg17) {
            args.Arg17.Add(k);
        }
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.MyService_method18_ParamClear(args);
        kratos.ObjectPoolPlain<MyService_method18_args>.Recycle(args);
        return buffer;
    }
}

}

namespace rpc {

internal class LuaCallParamSerializer {
    private static Google.Protobuf.MessageParser<Lua_login_args> Lua_login_args_parser = new Google.Protobuf.MessageParser<Lua_login_args>(()=>ObjectPoolPlain<Lua_login_args>.Get());
    private static Google.Protobuf.MessageParser<Lua_logout_args> Lua_logout_args_parser = new Google.Protobuf.MessageParser<Lua_logout_args>(()=>ObjectPoolPlain<Lua_logout_args>.Get());
    public static void login_deserialize(Lua_login_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Lua_login_args args = Lua_login_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = args.Arg1;
            param.arg2 = args.Arg2;
            PbParamClear.Lua_login_ParamClear(args);
            ObjectPoolPlain<Lua_login_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] login_serialize(Lua_login_CallParam param) {
        if (param.NoParam) { return null; }
        Lua_login_args args = kratos.ObjectPoolPlain<Lua_login_args>.Get();
        args.Arg1 = param.arg1;
        args.Arg2 = param.arg2;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Lua_login_ParamClear(args);
        kratos.ObjectPoolPlain<Lua_login_args>.Recycle(args);
        return buffer;
    }
    public static void logout_deserialize(Lua_logout_CallParam param, GrowingRingBuffer<byte> rb, uint length) {
        byte[] buffer = null;
        try {
            if (length == 0) { return; }
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            Lua_logout_args args = Lua_logout_args_parser.ParseFrom(buffer, 0, (int)length);
            param.arg1 = (ulong)args.Arg1;
            PbParamClear.Lua_logout_ParamClear(args);
            ObjectPoolPlain<Lua_logout_args>.Recycle(args);
        } finally {
            if (buffer != null) {
                kratos.GlobalBytePool.Pool.Return(buffer);
            }
        }
    }
    public static byte[] logout_serialize(Lua_logout_CallParam param) {
        if (param.NoParam) { return null; }
        Lua_logout_args args = kratos.ObjectPoolPlain<Lua_logout_args>.Get();
        args.Arg1 = param.arg1;
        int size = args.CalculateSize();
        param.Length = (uint)size;
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            args.WriteTo(stream);
        }
        PbParamClear.Lua_logout_ParamClear(args);
        kratos.ObjectPoolPlain<Lua_logout_args>.Recycle(args);
        return buffer;
    }
}

}

namespace rpc {

internal class ServiceCallReturnSerializer {
    private static Google.Protobuf.MessageParser<Service_method2_ret> Service_method2_ret_parser = new Google.Protobuf.MessageParser<Service_method2_ret>(()=>ObjectPoolPlain<Service_method2_ret>.Get());
    private static Google.Protobuf.MessageParser<Service_method3_ret> Service_method3_ret_parser = new Google.Protobuf.MessageParser<Service_method3_ret>(()=>ObjectPoolPlain<Service_method3_ret>.Get());
    public static void method1_deserialize(Service_method1_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method1_serialize(Service_method1_CallReturn param) {
        return null;
    }
    public static void method2_deserialize(Service_method2_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        Service_method2_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = Service_method2_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = ret.Ret;
        }
        PbRetClear.Service_method2_RetClear(ret);
        kratos.ObjectPoolPlain<Service_method2_ret>.Recycle(ret);
        ProxyCallAwaiter<string> waiter = (ProxyCallAwaiter<string>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method2_serialize(Service_method2_CallReturn param) {
        Service_method2_ret ret = ObjectPoolPlain<Service_method2_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.Service_method2_RetClear(ret);
        kratos.ObjectPoolPlain<Service_method2_ret>.Recycle(ret);
        return buffer;
    }
    public static void method3_deserialize(Service_method3_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        Service_method3_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = Service_method3_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new Dictionary<long,Dummy>();
        if (ret != null) {
            foreach (var k in ret.Ret) {
                param.ret.Add(k.Key, new DummyImpl(k.Value));
            }
        }
        PbRetClear.Service_method3_RetClear(ret);
        kratos.ObjectPoolPlain<Service_method3_ret>.Recycle(ret);
        ProxyCallAwaiter<Dictionary<long,Dummy>> waiter = (ProxyCallAwaiter<Dictionary<long,Dummy>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method3_serialize(Service_method3_CallReturn param) {
        Service_method3_ret ret = ObjectPoolPlain<Service_method3_ret>.Get();
        foreach (var k in param.ret) {
            ret.Ret.Add(k.Key, ((DummyImpl)(k.Value)).PBObject);
        }
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.Service_method3_RetClear(ret);
        kratos.ObjectPoolPlain<Service_method3_ret>.Recycle(ret);
        return buffer;
    }
    public static void method4_deserialize(Service_method4_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method4_serialize(Service_method4_CallReturn param) {
        return null;
    }
    public static void method5_deserialize(Service_method5_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method5_serialize(Service_method5_CallReturn param) {
        return null;
    }
}

}

namespace rpc {

internal class ServiceDynamicCallReturnSerializer {
    private static Google.Protobuf.MessageParser<ServiceDynamic_method2_ret> ServiceDynamic_method2_ret_parser = new Google.Protobuf.MessageParser<ServiceDynamic_method2_ret>(()=>ObjectPoolPlain<ServiceDynamic_method2_ret>.Get());
    private static Google.Protobuf.MessageParser<ServiceDynamic_method3_ret> ServiceDynamic_method3_ret_parser = new Google.Protobuf.MessageParser<ServiceDynamic_method3_ret>(()=>ObjectPoolPlain<ServiceDynamic_method3_ret>.Get());
    public static void method1_deserialize(ServiceDynamic_method1_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method1_serialize(ServiceDynamic_method1_CallReturn param) {
        return null;
    }
    public static void method2_deserialize(ServiceDynamic_method2_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        ServiceDynamic_method2_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = ServiceDynamic_method2_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = ret.Ret;
        }
        PbRetClear.ServiceDynamic_method2_RetClear(ret);
        kratos.ObjectPoolPlain<ServiceDynamic_method2_ret>.Recycle(ret);
        ProxyCallAwaiter<string> waiter = (ProxyCallAwaiter<string>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method2_serialize(ServiceDynamic_method2_CallReturn param) {
        ServiceDynamic_method2_ret ret = ObjectPoolPlain<ServiceDynamic_method2_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.ServiceDynamic_method2_RetClear(ret);
        kratos.ObjectPoolPlain<ServiceDynamic_method2_ret>.Recycle(ret);
        return buffer;
    }
    public static void method3_deserialize(ServiceDynamic_method3_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        ServiceDynamic_method3_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = ServiceDynamic_method3_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new Dictionary<long,Dummy>();
        if (ret != null) {
            foreach (var k in ret.Ret) {
                param.ret.Add(k.Key, new DummyImpl(k.Value));
            }
        }
        PbRetClear.ServiceDynamic_method3_RetClear(ret);
        kratos.ObjectPoolPlain<ServiceDynamic_method3_ret>.Recycle(ret);
        ProxyCallAwaiter<Dictionary<long,Dummy>> waiter = (ProxyCallAwaiter<Dictionary<long,Dummy>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method3_serialize(ServiceDynamic_method3_CallReturn param) {
        ServiceDynamic_method3_ret ret = ObjectPoolPlain<ServiceDynamic_method3_ret>.Get();
        foreach (var k in param.ret) {
            ret.Ret.Add(k.Key, ((DummyImpl)(k.Value)).PBObject);
        }
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.ServiceDynamic_method3_RetClear(ret);
        kratos.ObjectPoolPlain<ServiceDynamic_method3_ret>.Recycle(ret);
        return buffer;
    }
    public static void method4_deserialize(ServiceDynamic_method4_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method4_serialize(ServiceDynamic_method4_CallReturn param) {
        return null;
    }
    public static void method5_deserialize(ServiceDynamic_method5_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] method5_serialize(ServiceDynamic_method5_CallReturn param) {
        return null;
    }
}

}

namespace rpc {

internal class LoginCallReturnSerializer {
    private static Google.Protobuf.MessageParser<Login_login_ret> Login_login_ret_parser = new Google.Protobuf.MessageParser<Login_login_ret>(()=>ObjectPoolPlain<Login_login_ret>.Get());
    public static void login_deserialize(Login_login_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        Login_login_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = Login_login_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (ulong)ret.Ret;
        }
        PbRetClear.Login_login_RetClear(ret);
        kratos.ObjectPoolPlain<Login_login_ret>.Recycle(ret);
        ProxyCallAwaiter<ulong> waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] login_serialize(Login_login_CallReturn param) {
        Login_login_ret ret = ObjectPoolPlain<Login_login_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.Login_login_RetClear(ret);
        kratos.ObjectPoolPlain<Login_login_ret>.Recycle(ret);
        return buffer;
    }
    public static void logout_deserialize(Login_logout_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] logout_serialize(Login_logout_CallReturn param) {
        return null;
    }
}

}

namespace rpc {

internal class LuaLoginCallReturnSerializer {
    private static Google.Protobuf.MessageParser<LuaLogin_login_ret> LuaLogin_login_ret_parser = new Google.Protobuf.MessageParser<LuaLogin_login_ret>(()=>ObjectPoolPlain<LuaLogin_login_ret>.Get());
    public static void login_deserialize(LuaLogin_login_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        LuaLogin_login_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = LuaLogin_login_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (ulong)ret.Ret;
        }
        PbRetClear.LuaLogin_login_RetClear(ret);
        kratos.ObjectPoolPlain<LuaLogin_login_ret>.Recycle(ret);
        ProxyCallAwaiter<ulong> waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] login_serialize(LuaLogin_login_CallReturn param) {
        LuaLogin_login_ret ret = ObjectPoolPlain<LuaLogin_login_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.LuaLogin_login_RetClear(ret);
        kratos.ObjectPoolPlain<LuaLogin_login_ret>.Recycle(ret);
        return buffer;
    }
    public static void logout_deserialize(LuaLogin_logout_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] logout_serialize(LuaLogin_logout_CallReturn param) {
        return null;
    }
    public static void oneway_method_deserialize(LuaLogin_oneway_method_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] oneway_method_serialize(LuaLogin_oneway_method_CallReturn param) {
        return null;
    }
    public static void normal_method_deserialize(LuaLogin_normal_method_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] normal_method_serialize(LuaLogin_normal_method_CallReturn param) {
        return null;
    }
}

}

namespace rpc {

internal class MyServiceCallReturnSerializer {
    private static Google.Protobuf.MessageParser<MyService_method1_ret> MyService_method1_ret_parser = new Google.Protobuf.MessageParser<MyService_method1_ret>(()=>ObjectPoolPlain<MyService_method1_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method2_ret> MyService_method2_ret_parser = new Google.Protobuf.MessageParser<MyService_method2_ret>(()=>ObjectPoolPlain<MyService_method2_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method3_ret> MyService_method3_ret_parser = new Google.Protobuf.MessageParser<MyService_method3_ret>(()=>ObjectPoolPlain<MyService_method3_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method4_ret> MyService_method4_ret_parser = new Google.Protobuf.MessageParser<MyService_method4_ret>(()=>ObjectPoolPlain<MyService_method4_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method5_ret> MyService_method5_ret_parser = new Google.Protobuf.MessageParser<MyService_method5_ret>(()=>ObjectPoolPlain<MyService_method5_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method6_ret> MyService_method6_ret_parser = new Google.Protobuf.MessageParser<MyService_method6_ret>(()=>ObjectPoolPlain<MyService_method6_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method7_ret> MyService_method7_ret_parser = new Google.Protobuf.MessageParser<MyService_method7_ret>(()=>ObjectPoolPlain<MyService_method7_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method8_ret> MyService_method8_ret_parser = new Google.Protobuf.MessageParser<MyService_method8_ret>(()=>ObjectPoolPlain<MyService_method8_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method9_ret> MyService_method9_ret_parser = new Google.Protobuf.MessageParser<MyService_method9_ret>(()=>ObjectPoolPlain<MyService_method9_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method10_ret> MyService_method10_ret_parser = new Google.Protobuf.MessageParser<MyService_method10_ret>(()=>ObjectPoolPlain<MyService_method10_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method11_ret> MyService_method11_ret_parser = new Google.Protobuf.MessageParser<MyService_method11_ret>(()=>ObjectPoolPlain<MyService_method11_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method12_ret> MyService_method12_ret_parser = new Google.Protobuf.MessageParser<MyService_method12_ret>(()=>ObjectPoolPlain<MyService_method12_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method13_ret> MyService_method13_ret_parser = new Google.Protobuf.MessageParser<MyService_method13_ret>(()=>ObjectPoolPlain<MyService_method13_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method14_ret> MyService_method14_ret_parser = new Google.Protobuf.MessageParser<MyService_method14_ret>(()=>ObjectPoolPlain<MyService_method14_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method15_ret> MyService_method15_ret_parser = new Google.Protobuf.MessageParser<MyService_method15_ret>(()=>ObjectPoolPlain<MyService_method15_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method16_ret> MyService_method16_ret_parser = new Google.Protobuf.MessageParser<MyService_method16_ret>(()=>ObjectPoolPlain<MyService_method16_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method17_ret> MyService_method17_ret_parser = new Google.Protobuf.MessageParser<MyService_method17_ret>(()=>ObjectPoolPlain<MyService_method17_ret>.Get());
    private static Google.Protobuf.MessageParser<MyService_method18_ret> MyService_method18_ret_parser = new Google.Protobuf.MessageParser<MyService_method18_ret>(()=>ObjectPoolPlain<MyService_method18_ret>.Get());
    public static void method1_deserialize(MyService_method1_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method1_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method1_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (sbyte)ret.Ret;
        }
        PbRetClear.MyService_method1_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method1_ret>.Recycle(ret);
        ProxyCallAwaiter<sbyte> waiter = (ProxyCallAwaiter<sbyte>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method1_serialize(MyService_method1_CallReturn param) {
        MyService_method1_ret ret = ObjectPoolPlain<MyService_method1_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method1_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method1_ret>.Recycle(ret);
        return buffer;
    }
    public static void method2_deserialize(MyService_method2_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method2_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method2_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (short)ret.Ret;
        }
        PbRetClear.MyService_method2_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method2_ret>.Recycle(ret);
        ProxyCallAwaiter<short> waiter = (ProxyCallAwaiter<short>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method2_serialize(MyService_method2_CallReturn param) {
        MyService_method2_ret ret = ObjectPoolPlain<MyService_method2_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method2_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method2_ret>.Recycle(ret);
        return buffer;
    }
    public static void method3_deserialize(MyService_method3_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method3_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method3_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (int)ret.Ret;
        }
        PbRetClear.MyService_method3_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method3_ret>.Recycle(ret);
        ProxyCallAwaiter<int> waiter = (ProxyCallAwaiter<int>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method3_serialize(MyService_method3_CallReturn param) {
        MyService_method3_ret ret = ObjectPoolPlain<MyService_method3_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method3_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method3_ret>.Recycle(ret);
        return buffer;
    }
    public static void method4_deserialize(MyService_method4_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method4_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method4_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (long)ret.Ret;
        }
        PbRetClear.MyService_method4_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method4_ret>.Recycle(ret);
        ProxyCallAwaiter<long> waiter = (ProxyCallAwaiter<long>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method4_serialize(MyService_method4_CallReturn param) {
        MyService_method4_ret ret = ObjectPoolPlain<MyService_method4_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method4_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method4_ret>.Recycle(ret);
        return buffer;
    }
    public static void method5_deserialize(MyService_method5_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method5_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method5_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (byte)ret.Ret;
        }
        PbRetClear.MyService_method5_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method5_ret>.Recycle(ret);
        ProxyCallAwaiter<byte> waiter = (ProxyCallAwaiter<byte>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method5_serialize(MyService_method5_CallReturn param) {
        MyService_method5_ret ret = ObjectPoolPlain<MyService_method5_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method5_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method5_ret>.Recycle(ret);
        return buffer;
    }
    public static void method6_deserialize(MyService_method6_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method6_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method6_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (ushort)ret.Ret;
        }
        PbRetClear.MyService_method6_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method6_ret>.Recycle(ret);
        ProxyCallAwaiter<ushort> waiter = (ProxyCallAwaiter<ushort>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method6_serialize(MyService_method6_CallReturn param) {
        MyService_method6_ret ret = ObjectPoolPlain<MyService_method6_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method6_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method6_ret>.Recycle(ret);
        return buffer;
    }
    public static void method7_deserialize(MyService_method7_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method7_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method7_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (uint)ret.Ret;
        }
        PbRetClear.MyService_method7_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method7_ret>.Recycle(ret);
        ProxyCallAwaiter<uint> waiter = (ProxyCallAwaiter<uint>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method7_serialize(MyService_method7_CallReturn param) {
        MyService_method7_ret ret = ObjectPoolPlain<MyService_method7_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method7_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method7_ret>.Recycle(ret);
        return buffer;
    }
    public static void method8_deserialize(MyService_method8_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method8_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method8_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (ulong)ret.Ret;
        }
        PbRetClear.MyService_method8_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method8_ret>.Recycle(ret);
        ProxyCallAwaiter<ulong> waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method8_serialize(MyService_method8_CallReturn param) {
        MyService_method8_ret ret = ObjectPoolPlain<MyService_method8_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method8_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method8_ret>.Recycle(ret);
        return buffer;
    }
    public static void method9_deserialize(MyService_method9_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method9_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method9_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (bool)ret.Ret;
        }
        PbRetClear.MyService_method9_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method9_ret>.Recycle(ret);
        ProxyCallAwaiter<bool> waiter = (ProxyCallAwaiter<bool>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method9_serialize(MyService_method9_CallReturn param) {
        MyService_method9_ret ret = ObjectPoolPlain<MyService_method9_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method9_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method9_ret>.Recycle(ret);
        return buffer;
    }
    public static void method10_deserialize(MyService_method10_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method10_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method10_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = ret.Ret;
        }
        PbRetClear.MyService_method10_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method10_ret>.Recycle(ret);
        ProxyCallAwaiter<string> waiter = (ProxyCallAwaiter<string>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method10_serialize(MyService_method10_CallReturn param) {
        MyService_method10_ret ret = ObjectPoolPlain<MyService_method10_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method10_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method10_ret>.Recycle(ret);
        return buffer;
    }
    public static void method11_deserialize(MyService_method11_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method11_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method11_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (float)ret.Ret;
        }
        PbRetClear.MyService_method11_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method11_ret>.Recycle(ret);
        ProxyCallAwaiter<float> waiter = (ProxyCallAwaiter<float>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method11_serialize(MyService_method11_CallReturn param) {
        MyService_method11_ret ret = ObjectPoolPlain<MyService_method11_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method11_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method11_ret>.Recycle(ret);
        return buffer;
    }
    public static void method12_deserialize(MyService_method12_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method12_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method12_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (double)ret.Ret;
        }
        PbRetClear.MyService_method12_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method12_ret>.Recycle(ret);
        ProxyCallAwaiter<double> waiter = (ProxyCallAwaiter<double>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method12_serialize(MyService_method12_CallReturn param) {
        MyService_method12_ret ret = ObjectPoolPlain<MyService_method12_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method12_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method12_ret>.Recycle(ret);
        return buffer;
    }
    public static void method13_deserialize(MyService_method13_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method13_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method13_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new DataImpl();
        if (ret != null) {
            ((DataImpl)(param.ret)).PBObject = ret.Ret;
        }
        PbRetClear.MyService_method13_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method13_ret>.Recycle(ret);
        ProxyCallAwaiter<Data> waiter = (ProxyCallAwaiter<Data>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method13_serialize(MyService_method13_CallReturn param) {
        MyService_method13_ret ret = ObjectPoolPlain<MyService_method13_ret>.Get();
        ret.Ret = ((DataImpl)(param.ret)).PBObject;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method13_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method13_ret>.Recycle(ret);
        return buffer;
    }
    public static void method14_deserialize(MyService_method14_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method14_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method14_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new DummyImpl();
        if (ret != null) {
            ((DummyImpl)(param.ret)).PBObject = ret.Ret;
        }
        PbRetClear.MyService_method14_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method14_ret>.Recycle(ret);
        ProxyCallAwaiter<Dummy> waiter = (ProxyCallAwaiter<Dummy>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method14_serialize(MyService_method14_CallReturn param) {
        MyService_method14_ret ret = ObjectPoolPlain<MyService_method14_ret>.Get();
        ret.Ret = ((DummyImpl)(param.ret)).PBObject;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method14_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method14_ret>.Recycle(ret);
        return buffer;
    }
    public static void method15_deserialize(MyService_method15_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method15_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method15_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new List<Data>();
        if (ret != null) {
            foreach (var k in ret.Ret) {
                param.ret.Add(new DataImpl(k));
            }
        }
        PbRetClear.MyService_method15_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method15_ret>.Recycle(ret);
        ProxyCallAwaiter<List<Data>> waiter = (ProxyCallAwaiter<List<Data>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method15_serialize(MyService_method15_CallReturn param) {
        MyService_method15_ret ret = ObjectPoolPlain<MyService_method15_ret>.Get();
        foreach (var k in param.ret) {
            ret.Ret.Add(((DataImpl)k).PBObject);
        }
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method15_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method15_ret>.Recycle(ret);
        return buffer;
    }
    public static void method16_deserialize(MyService_method16_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method16_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method16_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new Dictionary<uint,Data>();
        if (ret != null) {
            foreach (var k in ret.Ret) {
                param.ret.Add(k.Key, new DataImpl(k.Value));
            }
        }
        PbRetClear.MyService_method16_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method16_ret>.Recycle(ret);
        ProxyCallAwaiter<Dictionary<uint,Data>> waiter = (ProxyCallAwaiter<Dictionary<uint,Data>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method16_serialize(MyService_method16_CallReturn param) {
        MyService_method16_ret ret = ObjectPoolPlain<MyService_method16_ret>.Get();
        foreach (var k in param.ret) {
            ret.Ret.Add(k.Key, ((DataImpl)(k.Value)).PBObject);
        }
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method16_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method16_ret>.Recycle(ret);
        return buffer;
    }
    public static void method17_deserialize(MyService_method17_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method17_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method17_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        param.ret = new HashSet<string>();
        if (ret != null) {
            foreach (var k in ret.Ret) {
                param.ret.Add(k);
            }
        }
        PbRetClear.MyService_method17_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method17_ret>.Recycle(ret);
        ProxyCallAwaiter<HashSet<string>> waiter = (ProxyCallAwaiter<HashSet<string>>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method17_serialize(MyService_method17_CallReturn param) {
        MyService_method17_ret ret = ObjectPoolPlain<MyService_method17_ret>.Get();
        foreach (var k in param.ret) {
            ret.Ret.Add(k);
        }
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method17_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method17_ret>.Recycle(ret);
        return buffer;
    }
    public static void method18_deserialize(MyService_method18_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        MyService_method18_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = MyService_method18_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (int)ret.Ret;
        }
        PbRetClear.MyService_method18_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method18_ret>.Recycle(ret);
        ProxyCallAwaiter<int> waiter = (ProxyCallAwaiter<int>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] method18_serialize(MyService_method18_CallReturn param) {
        MyService_method18_ret ret = ObjectPoolPlain<MyService_method18_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.MyService_method18_RetClear(ret);
        kratos.ObjectPoolPlain<MyService_method18_ret>.Recycle(ret);
        return buffer;
    }
}

}

namespace rpc {

internal class LuaCallReturnSerializer {
    private static Google.Protobuf.MessageParser<Lua_login_ret> Lua_login_ret_parser = new Google.Protobuf.MessageParser<Lua_login_ret>(()=>ObjectPoolPlain<Lua_login_ret>.Get());
    public static void login_deserialize(Lua_login_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
        if (length == 0) { return; }
        byte[] buffer = null;
        Lua_login_ret ret = null;
        try {
            buffer = GlobalBytePool.Pool.Rent((int)length);
            for (int i = 0; i < (int)length; i++) {
                buffer[i] = rb.Get();
            }
            ret = Lua_login_ret_parser.ParseFrom(buffer, 0, (int)length);
        } finally {
            GlobalBytePool.Pool.Return(buffer);
        }
        if (ret != null) {
            param.ret = (ulong)ret.Ret;
        }
        PbRetClear.Lua_login_RetClear(ret);
        kratos.ObjectPoolPlain<Lua_login_ret>.Recycle(ret);
        ProxyCallAwaiter<ulong> waiter = (ProxyCallAwaiter<ulong>)Singleton<ManagerThreaded<uint, ProxyCall>>.Instance.Get(param.CallID);
        if (waiter != null) {
            waiter.SetResult(param.ret);
        }
    }
    public static byte[] login_serialize(Lua_login_CallReturn param) {
        Lua_login_ret ret = ObjectPoolPlain<Lua_login_ret>.Get();
        ret.Ret = param.ret;
        int size = ret.CalculateSize();
        param.Length = (uint)size; 
        byte[] buffer = GlobalBytePool.Pool.Rent(size);
        using (CodedOutputStream stream = new CodedOutputStream(buffer)) {
            ret.WriteTo(stream);
        }
        PbRetClear.Lua_login_RetClear(ret);
        kratos.ObjectPoolPlain<Lua_login_ret>.Recycle(ret);
        return buffer;
    }
    public static void logout_deserialize(Lua_logout_CallReturn param, GrowingRingBuffer<byte> rb, uint length) {
    }
    public static byte[] logout_serialize(Lua_logout_CallReturn param) {
        return null;
    }
}

}

#endregion Machine generated code

