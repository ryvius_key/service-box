function test()
end

function on_tick(now)
    test()
end

function switch_refresh_cb()
   return "lua_switch_name", true, "i am a tips"
end

function switch_change_cb(on_off)
   return true, ""
end

function selection_refresh_cb()
    return "lua_selection_name", "i am a tips", {"a", "b", "c"}
 end
 
 function selection_change_cb(name)
    return true, ""
 end

function on_after_fork()
    ctx:log_verb("on_after_fork")
    ctx.console:add_switch("lua_switch_name", "tips", switch_refresh_cb, switch_change_cb)
    ctx.console:add_selection("lua_selection_name", "tips", selection_refresh_cb, selection_change_cb)
    return true
end

function on_before_destroy()
    ctx:log_verb("on_before_destroy")
end

StubLua={}

function StubLua:login(arg1,arg2)
    return nil
end

function StubLua:logout(arg1)
end

