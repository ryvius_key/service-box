// Machine generated code

#include "example.service.LuaLogin.impl.h"
#include "object_pool.h"
#include "rpc_singleton.h"
#include <utility>

#include "../util/lua/lua_service.hh"

LuaLoginImpl::LuaLoginImpl() {}
LuaLoginImpl::~LuaLoginImpl() {}
bool LuaLoginImpl::onAfterFork(rpc::Rpc *rpc) {
  lua_service_ = getContext()->new_lua_service();
  return lua_service_->start(getServiceUUID());
}
bool LuaLoginImpl::onBeforeDestory(rpc::Rpc *rpc) {
  return lua_service_->stop();
}
void LuaLoginImpl::onTick(std::time_t ms) { lua_service_->update(ms); }
void LuaLoginImpl::onServiceCall(rpc::StubCallPtr callPtr) {
  lua_service_->call(callPtr);
}
