#pragma once

#include "example.service.ServiceDynamic.h"
#include "Login/proxy/example.service.Login.proxy.h"
#include "service_context.hh"

class ServiceDynamicImpl : public ServiceDynamic {
  LoginPrx login_prx;
  kratos::service::LoggerPtr logger;
  kratos::service::SchedulePtr scheduler;
  std::size_t forward_call_count{0};
  std::size_t backward_call_count{0};
  kratos::service::BoxConsolePtr console;
public:
//implementation->
    ServiceDynamicImpl();
    virtual ~ServiceDynamicImpl();
    virtual bool onAfterFork(rpc::Rpc* rpc) override;
    virtual bool onBeforeDestory(rpc::Rpc* rpc) override;
    virtual void onTick(std::time_t ms) override;
    virtual void method1(rpc::StubCallPtr call, const Data&, const std::string&) override;
    virtual std::shared_ptr<std::string> method2(rpc::StubCallPtr call, std::int8_t, const std::unordered_set<std::string>&, std::uint64_t) override;
    virtual std::shared_ptr<std::unordered_map<std::int64_t,Dummy>> method3(rpc::StubCallPtr call, std::int8_t, const std::unordered_set<std::string>&, const std::unordered_map<std::int64_t,std::string>&, std::uint64_t) override;
    virtual void method4(rpc::StubCallPtr call) override;
    virtual void method5(rpc::StubCallPtr call) override;
//implementation<-
};

