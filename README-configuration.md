# 配置service box

TODO 说明

# 配置节点

1. listener
2. service_finder
3. service
4. command
5. debugger
6. statistics
7. proxy
8. http
9. 全局属性

## listener
1. listener.host
```
listener = {
    host = ("0.0.0.0:6888")
}
```

## service_finder
1. service_finder.type
2. service_finder.hosts

```
service_finder = {
    type = "zookeeper"
    hosts = "127.0.0.1:2181"
}
```

## service
1. service.service_dir
2. service.preload_service
3. service.remote_service_repo_version_api
4. service.remote_service_repo_dir
5. service.remote_service_repo_latest_version_api
6. service.is_open_remote_update
7. service.remote_repo_check_interval
8. service.is_start_as_daemon
```
service = {
    service_dir="../../service/Debug"
    preload_service=<5871407834711899994:"libServiceDynamic.so",5871407834537456905:"libLogin.so",5871407834107390365:"libMyService.so",5871407833380299500:"libLua.so">
    remote_service_repo_version_api = "http://localhost/doc/version.json"
    remote_service_repo_dir = "http://localhost/doc"
    remote_service_repo_latest_version_api = ""
    is_open_remote_update = "false"
    remote_repo_check_interval = 60
    is_start_as_daemon = "false"
}
```

## command
1. command.enable
2. command.listen
3. command.root_page_path

```
command = {
    enable = "true"
    listen = "0.0.0.0:6889"
    root_page_path = "root.html"
}

```

## debugger
1. debugger.enable
2. debugger.listen

```
debugger = {
    enable = "true"
    listen = "0.0.0.0:10888"
}
```

## statistics
1. statistics.is_open_rpc_stat

```
statistics = {
    is_open_rpc_stat = "true"
}
```

## proxy
1. proxy.listener
2. proxy.seed
3. proxy.query_timeout
4. proxy.call_timeout
```
proxy = {
    query_timeout = 5000
    call_timeout = 2000
    seed = 1
    listener = ("0.0.0.0:9888")
}
```
## http
1. http.http_max_call_timeout

```
http = {
    http.http_max_call_timeout = 5000
}
```

## 全局属性
1. box_channel_recv_buffer_len
2. box_name
3. logger_config_line
4. connect_other_box_timeout
5. service_finder_connect_timeout
6. necessary_service
7. open_coroutine

```
box_channel_recv_buffer_len = 102400
box_name = "service_box"
logger_config_line = "file://.;file=box_%y%m%d.log;line=[%y%m%d-%H:%M:%S:%U];flush=true;mode=day;maxSize=10240000"
connect_other_box_timeout = 2000
service_finder_connect_timeout = 2000
necessary_service = ()
open_coroutine = "true"
```