#!/bin/sh

cd ..\src
cmake .
make
cd ..\publish\bin
python pub.py -c ..\config\box.json
cd box
./box -c box.cfg
