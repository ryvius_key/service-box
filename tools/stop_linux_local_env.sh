#!/bin/sh

sudo systemctl stop nginx
sudo systemctl stop redis

cd ../build/service-box-linux-dependency/zookeeper-bin/bin
./zkServer.sh stop
cd ../../../tools
