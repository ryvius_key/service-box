/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sstream>

#include "number_impl.h"

template <typename T> T lexical_cast(const std::string &s) {
  T n;
  std::stringstream ss;
  ss << s;
  ss >> n;
  return n;
}

NumberImpl::NumberImpl(const std::string &value) { _value = value; }

NumberImpl::~NumberImpl() {}

int NumberImpl::getInt() {
  int value = 0;

  try {
    value = lexical_cast<int>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

const std::string &NumberImpl::getString() { return _value; }

unsigned int NumberImpl::getUint() {
  unsigned int value = 0;

  try {
    value = lexical_cast<unsigned int>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

long NumberImpl::getLong() {
  long value = 0;

  try {
    value = lexical_cast<long>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

unsigned long NumberImpl::getUlong() {
  unsigned long value = 0;

  try {
    value = lexical_cast<unsigned long>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

long long NumberImpl::getLlong() {
  long long value = 0;

  try {
    value = lexical_cast<long long>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

unsigned long long NumberImpl::getULlong() {
  unsigned long long value = 0;

  try {
    value = lexical_cast<unsigned long long>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

float NumberImpl::getFloat() {
  float value = 0;

  try {
    value = lexical_cast<float>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}

double NumberImpl::getDouble() {
  double value = 0;

  try {
    value = lexical_cast<double>(_value);
  } catch (std::exception &) {
    throw ConfigException("invalid conversion");
  }

  return value;
}
