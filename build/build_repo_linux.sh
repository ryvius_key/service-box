#!/bin/sh

yum install redis -y
chown redis:redis /var/log/redis/redis.log

yum install nginx -y

cd service-box-linux-dependency
cp google /usr/local/include/ -rf
cp hiredis /usr/local/include/ -rf
cp protobuf.pc /usr/local/lib/pkgconfig/ -rf
cp libprotobuf.a /usr/local/lib -rf
cp protoc /usr/local/bin/ -rf
cp libcurl.a /usr/local/lib -rf
cp libhashtable.a /usr/local/lib -rf
cp libhiredis.a /usr/local/lib -rf
cp libz.a /usr/local/lib -rf
cp libzookeeper.a /usr/local/lib -rf

cd ../../src/repo
python repo.py -t cpp -i

cd ../
cmake .
make

cd ./repo
python repo.py -t cpp -a example/example.idl
python repo.py -t cpp -b example
python repo.py -t lua -i
python repo.py -t lua -u example
python repo.py -t lua -b example

cd ../../build
