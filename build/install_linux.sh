#!/bin/sh

#change source to aliyun 
echo "backpack source file"
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.back
#download source
wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
yum makecache

yum install -y git

python build_local_env_linux.py

chmod +x ./initgcc.sh
chmod +x ./build_repo_linux.sh
chmod +x ../tools/start_linux_box.sh
chmod +x ../tools/start_linux_local_env.sh
chmod +x ../tools/stop_linux_local_env.sh

./initgcc.sh

if [ $? gt 0]
then
        echo "install gcc init error"
        exit 1
fi 

./build_repo_linux.sh

yum install java -y
