#!/bin/sh

function installcmake(){
    wget https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz
    tar zxvf cmake-3.10.0.tar.gz
    cd cmake-3.10.0
    ./bootstrap
    gmake -j8
    make install
}

#root 权限检测
function root_need() {
    if [[ $EUID -ne 0 ]]; then
        echo "Error:This script must be run as root!" 1>&2
        exit 1
    fi
}

#前置环境安装，编译需要旧环境
function initenv(){
    echo ===========start install gcc=================
    yum install curl-devel -y
    yum install zlib-devel -y
    yum install bzip2 -y
    yum install -y gcc
    yum install -y gcc-c++
}

#下载gcc 9.2 源码
function down_loadgcc(){
    echo "gcc 92 不存在 开始从ftp下载"
    wget http://mirror.hust.edu.cn/gnu/gcc/gcc-9.2.0/gcc-9.2.0.tar.gz
    if [ $? -gt 0 ]
    then
        echo "下载失败请手动下载gcc 在执行脚本"
        exit -1
    fi
}

#安装脚本
function install_gcc(){
    echo "开始解压"

    if [ ! -d ./gcc-9.2.0 ]
    then
        tar zxvf gcc-9.2.0.tar.gz
        if [ $? -gt -0 ]
        then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd gcc-9.2.0

    #下载依赖文件
    echo "./contrib/download_prerequisites "
    ./contrib/download_prerequisites
    if [ $? -gt -0 ]
    then
        echo "./contrib/download_prerequisites 执行失败"
        exit 1
    fi 

    if [ ! -d ./build ]
    then
        mkdir build 
    fi 

    cd ./build
    echo "开始配置 configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++"
    ../configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++

    if [ $? -gt -0 ]
    then
        echo "./configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++ 执行失败，检查log文件"
        exit 1
    fi 

    make -j4
    make install 
    
    if [ $? -gt -0 ]
    then
        echo "安装失败"
        exit 1
    fi 
    
    # 环境变量path
    echo  "export PATH=/usr/local/gcc/bin:/usr/local/bin:$PATH" >> /etc/profile.d/gcc.sh
    source /etc/profile.d/gcc.sh

    # 头文件
    ln -sv /usr/local/gcc/include/ /usr/include/gcc

    # 库文件
    echo "/usr/local/gcc/lib64" >> /etc/ld.so.conf.d/gcc.conf
    ldconfig -v
    ldconfig -p |grep gcc

    echo "卸载原版gcc" 
    yum remove -y gcc 
}


#检查目录
echo "请使用root用户运行此脚本"
root_need

initenv

if ! [ -x "$(command -v cmake)" ]; then
installcmake
fi

echo "开始下载gcc 编译版本 请保证data 目录下有有分配足够空间"
if [ ! -d /data ]
then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -d /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

# download gcc from thirparty
cd /data/thirdparty 

if [ ! -f gcc-9.2.0.tar.gz ]
then
    down_loadgcc
fi

echo "开始md5 校验: "
MD5NUM=`md5sum gcc-9.2.0.tar.gz | awk '{print $1}'`

if [  "$MD5NUM" != "e03739b042a14376d727ddcfd05a9bc3" ]
then
    echo "md5 不匹配 尝试重新下载"
    rm gcc-9.2.0.tar.gz
    down_loadgcc
fi

#编译安装
install_gcc

#gcc 安装完毕 
echo "gcc 安装完毕请手动执行 source /etc/profile.d/gcc.sh  刷新 gcc 环境 "
ln -s /usr/local/gcc/bin/gcc /usr/bin/cc
ln -s /usr/local/gcc/bin/g++ /usr/bin/c++